package polytech.tours.di.projet.java.pointeuse.model.TCP;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import polytech.tours.di.projet.java.app.centrale.model.company.ConfigV2;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;
import polytech.tours.di.projet.java.pointeuse.controler.PointingMachineController;

/**
 * class that allow to send pointing packet to the main application by TCT protocol
 * implements Runnable interface
 * @author donovan
 *
 */
public class TCPClientPointing implements Runnable{

	/** attributes */
	private Socket s; /** < the socket */
	private ObjectOutputStream oos; /** < the writing stream */
	private Pointing p; /** < the pointing */
	private PointingMachineController controller;

	/**
	 * the constructor
	 * @param p
	 */
	public TCPClientPointing(Pointing p, PointingMachineController controller) { 		
		this.p = p;
		this.controller = controller;
	} 
	
	/**
	 * the run method of the runnable interface
	 * set connection and write a poitning in the TCP Stream to the company
	 */
	@Override
	public void run() {
		System.out.println("\nTCPClientPointing launched");
		try { 
			s = new Socket(ConfigV2.getIpAdress(), ConfigV2.getPortServerPointing()); 
			oos = new ObjectOutputStream(s.getOutputStream());
			oos.writeObject(p);
			System.out.println("\nTCPClientPointing writing done.");
			getS().close();
			oos.close();
		} catch(IOException e) 
		{ 
			System.out.println("\nwaiting for connection..."); 
			controller.getModel().addPointing(p);
		}
		
	}




	/**
	 * @return the s
	 */
	public Socket getS() {
		return s;
	}

	/**
	 * @param s the s to set
	 */
	public void setS(Socket s) {
		this.s = s;
	} 

}
