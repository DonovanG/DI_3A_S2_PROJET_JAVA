package polytech.tours.di.projet.java.pointeuse.model;

import polytech.tours.di.projet.java.app.centrale.model.company.ConfigV2;
import polytech.tours.di.projet.java.pointeuse.controler.PointingMachineController;

/**
 * the main method of the pointing application
 * @author donovan
 *
 */
public class MainPointingMachine {

	/**
	 * the main method
	 * @param args
	 */
	public static void main(String[] args) {
		
		/** the pointeuse model */
		Pointeuse pointeuse = new Pointeuse();
		
		ConfigV2.update();
		
		 @SuppressWarnings("unused")
		/** the controller */
		PointingMachineController controler  = new PointingMachineController(pointeuse);
	}
	
}
