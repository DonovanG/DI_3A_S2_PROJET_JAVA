package polytech.tours.di.projet.java.pointeuse.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;

import polytech.tours.di.projet.java.app.centrale.model.company.Employee;
import polytech.tours.di.projet.java.app.centrale.model.time.RoundedClock;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;

/**
 * The model of the pointing application
 * @author donovan
 */
public class Pointeuse extends Observable implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4347971904148570221L;
	/** attributes */
	private ArrayList<Employee> employees;
	private ArrayList<Pointing> pointings;
	private transient RoundedClock horloge;
	private transient TimeCheck timeCheck;
	
	/** constructor */
	public Pointeuse(){
		employees = new ArrayList<Employee>();
		pointings = new ArrayList<Pointing>();
		horloge = new RoundedClock();
		TimeCheck timeCheck = new TimeCheck();
		timeCheck.start();
	}
	
	public Pointeuse(Pointeuse pointeuse){
		employees = pointeuse.employees;
		pointings = pointeuse.pointings;
		horloge = new RoundedClock();
		TimeCheck timeCheck = new TimeCheck();
		timeCheck.start();
		
	}
	
	/**
	 * new thread that allow to update the clock o the view
	 * @author donovan
	 */
	private class TimeCheck extends Thread{
		/**
		 * run method : update clock every 5 minutes
		 */
		@Override
		public void run()
		{
			while(true)
			{
				try
				{
					Thread.sleep(30000);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				horloge = new RoundedClock();
				setChanged();
				notifyObservers(horloge);
			}
		}
	}
	
	/**
	 * @return the employees
	 */
	public ArrayList<Employee> getEmployees() {
		return employees;
	}
	/**
	 * @param employees the employees to set
	 */
	public void setEmployees(ArrayList<Employee> employees) {
		this.employees = employees;
		setChanged();
		notifyObservers(employees);
	}
	/**
	 * @return the pointings
	 */
	public ArrayList<Pointing> getPointings() {
		return pointings;
	}
	/**
	 * @param pointings the pointings to set
	 */
	public void setPointings(ArrayList<Pointing> pointings) {
		this.pointings = pointings;
	}
	
	/**
	 * add pointing to the pointing list
	 * @param p
	 */
	public void addPointing(Pointing p){
		if(!existPointing(p))
			pointings.add(p);
	}
	
	public boolean existPointing(Pointing p){
		for(Pointing pointing : pointings){
			if(pointing.equals(p))
				return true;
		}
		return false;
	}
	
    /**
     * get an employee of the company by he's name
     * @param name
     * @param firstName
     * @return e : the employee
     */
	public Employee getEmployee(String name, String firstName){
		for(Employee e : employees){
			if((e.getName()).equals(name) && (e.getFirstName()).equals(firstName))
				return e;
		}
		return null;
	}

	/**
	 * @return the timeCheck
	 */
	public TimeCheck getTimeCheck() {
		return timeCheck;
	}

	/**
	 * @param timeCheck the timeCheck to set
	 */
	public void setTimeCheck(TimeCheck timeCheck) {
		this.timeCheck = timeCheck;
	}}
