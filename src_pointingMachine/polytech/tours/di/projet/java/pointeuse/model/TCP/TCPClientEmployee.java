package polytech.tours.di.projet.java.pointeuse.model.TCP;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import polytech.tours.di.projet.java.app.centrale.model.company.ConfigV2;
import polytech.tours.di.projet.java.app.centrale.model.company.Employee;

/**
 * the TCP client the send a request to receive the company's list of employees
 * implement the callable interface
 * @author donovan
 *
 */
public class TCPClientEmployee implements Callable<ArrayList<Employee>>{
	
	/** attributes declarations */
	private Socket s;  /** < the socket */
	private ObjectInputStream ois;  /** < the reading stream */
	private ArrayList<Employee> employees;  /** < the employees' list */

	/**
	 * the call method of the allable interface
	 * set some connections and read/return the employees' list from the company by TCP protocol
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Employee> call() {
		try { 
			s = new Socket(ConfigV2.getIpAdress(), ConfigV2.getPortServerEmployee()); 
			System.out.println("TCPClientLaunched");
			ois = new ObjectInputStream(s.getInputStream());
			
			employees = ((ArrayList<Employee>) ois.readObject());
			
			s.close();
			ois.close();
			
		} catch(IOException e) 
		{ 
			System.out.println("IOException TCPClient"); 
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return employees;
	}
	

}
