/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polytech.tours.di.projet.java.pointeuse.view;

import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import polytech.tours.di.projet.java.app.centrale.model.company.Employee;
import polytech.tours.di.projet.java.app.centrale.model.time.RoundedClock;
import polytech.tours.di.projet.java.pointeuse.controler.PointingMachineController;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JComboBox;

/**
 * the view of the pointing application
 * @author donovan
 */
public class PointingMachineWindow extends javax.swing.JFrame implements Observer{


	/**
	 * 
	 */
	private static final long serialVersionUID = -7882604745153485156L;
	private PointingMachineController controler;
	
	/**
     * Creates new form PointeuseWindow
     */
    @SuppressWarnings("deprecation")
	public PointingMachineWindow(PointingMachineController controler) {
        this.controler = controler;
    	initComponents();
        Rectangle bounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        setLocation(bounds.x+bounds.width-size().width, bounds.y);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        synchronizedBouton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        checkBouton = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(196, 183, 170));

        synchronizedBouton.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        synchronizedBouton.setText("Synchronized Employees");
        synchronizedBouton.addActionListener(controler);

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel1.setText("Employee :");

        checkBouton.setText("Check In/Out");
        checkBouton.addActionListener(controler);

        jLabel4.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel4.setText((new RoundedClock()).getLdt().toLocalDate().toString());

        jLabel5.setFont(new java.awt.Font("Ubuntu", 0, 48)); // NOI18N
        jLabel5.setText((new RoundedClock()).getLdt().toLocalTime().toString());
        
        for(Employee e : this.controler.getModel().getEmployees())
        	comboBox.addItem(e.getName()+" "+e.getFirstName());
        
        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1Layout.setHorizontalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addGap(27)
        					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING, false)
        						.addComponent(comboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        						.addComponent(jLabel1, Alignment.LEADING)
        						.addComponent(synchronizedBouton, GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE))
        					.addGap(155)
        					.addComponent(checkBouton, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE))
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addGap(230)
        					.addComponent(jLabel5)))
        			.addGap(34))
        		.addGroup(Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
        			.addContainerGap(222, Short.MAX_VALUE)
        			.addComponent(jLabel4, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
        			.addGap(230))
        );
        jPanel1Layout.setVerticalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addGap(27)
        			.addComponent(jLabel4)
        			.addPreferredGap(ComponentPlacement.UNRELATED)
        			.addComponent(jLabel5)
        			.addPreferredGap(ComponentPlacement.RELATED, 82, Short.MAX_VALUE)
        			.addComponent(synchronizedBouton, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
        			.addGap(18)
        			.addComponent(jLabel1)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        				.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        				.addComponent(checkBouton, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE))
        			.addGap(57))
        );
        jPanel1.setLayout(jPanel1Layout);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE)
        );
        getContentPane().setLayout(layout);

        addWindowListener(controler);

        pack();
    }// </editor-fold>                        

    /**
     * the update method called by the method notify in the model
     * allow to refresh the view for the hour and whan the user click on a button
     */
	@Override
	public void update(Observable o, Object arg) {

		/** refresh the clock */
		if(arg instanceof RoundedClock){
			jLabel4.setText(((RoundedClock) arg).getLdt().toLocalDate().toString());
			jLabel5.setText(((RoundedClock) arg).getLdt().toLocalTime().toString());

		}
		/** refresh the combo box */
		else
			fillComboBox(arg);
			
	}                                

	/**
	 * allow to refresh the combo box with the new employees' list 
	 * @param arg
	 */
    @SuppressWarnings("unchecked")
	private void fillComboBox(Object arg) {
    	comboBox.removeAllItems();
        comboBox.addItem("--employees--");
        for(Employee e : ((ArrayList<Employee>) arg))
        	comboBox.addItem(e.getName()+" "+e.getFirstName());
	}

	// Variables declaration - do not modify 
    private JComboBox<String> comboBox = new JComboBox<String>();
    private javax.swing.JButton checkBouton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton synchronizedBouton;
    // End of variables declaration                   

    /** components getters and setters */
	public javax.swing.JButton getCheckBouton() {
		return checkBouton;
	}

	public void setCheckBouton(javax.swing.JButton checkBouton) {
		this.checkBouton = checkBouton;
	}

	public javax.swing.JLabel getjLabel4() {
		return jLabel4;
	}

	public void setjLabel4(javax.swing.JLabel jLabel4) {
		this.jLabel4 = jLabel4;
	}

	public javax.swing.JLabel getjLabel5() {
		return jLabel5;
	}

	public void setjLabel5(javax.swing.JLabel jLabel5) {
		this.jLabel5 = jLabel5;
	}

	public javax.swing.JButton getSynchronizedBouton() {
		return synchronizedBouton;
	}

	public void setSynchronizedBouton(javax.swing.JButton synchronizedBouton) {
		this.synchronizedBouton = synchronizedBouton;
	}

	public JComboBox<String> getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox<String> comboBox) {
		this.comboBox = comboBox;
	}
}
