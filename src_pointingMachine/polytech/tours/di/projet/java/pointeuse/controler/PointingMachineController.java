package polytech.tours.di.projet.java.pointeuse.controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import polytech.tours.di.projet.java.app.centrale.model.company.Employee;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;
import polytech.tours.di.projet.java.app.centrale.model.time.RoundedClock;
import polytech.tours.di.projet.java.pointeuse.model.Pointeuse;
import polytech.tours.di.projet.java.pointeuse.model.TCP.TCPClientEmployee;
import polytech.tours.di.projet.java.pointeuse.model.TCP.TCPClientPointing;
import polytech.tours.di.projet.java.pointeuse.view.PointingMachineWindow;

/**
 * the controller of the pointing application
 * @author donovan
 */
public class PointingMachineController implements ActionListener, WindowListener {

	/** attributes */
   private Pointeuse model;
   private PointingMachineWindow view;
   
   private Pointing p;
   private Employee e;
   
   private WaitingPointingsCheck waitingPointingsCheck;
   
	/**initialize objects for the serialization */
	ObjectOutputStream oos = null;
	ObjectInputStream ois = null;
	
	/** serialize file name**/
	String serializeFileName = "pointeuse.ser";
   
   /**
    * the constructor
    * @param pointeuse : the model
    */
   public PointingMachineController(Pointeuse pointeuse){
	   
       	/** initialize input stream for deserialization */
   		try {
	    	ois = new ObjectInputStream(new FileInputStream(serializeFileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
   		
 	   /** initialize the model */
		try {
			this.model = new Pointeuse((Pointeuse) ois.readObject());
		} catch (ClassNotFoundException | IOException e1) {
			e1.printStackTrace();
		}
	   
	   /** initialize the view */
	   this.view = new PointingMachineWindow(this);
	   this.view.setVisible(true);
	   
	   /** add the view as observer in the model */
	   this.model.addObserver(view);
	   
	   waitingPointingsCheck  = new WaitingPointingsCheck(this);
	   waitingPointingsCheck.start();
	   
		
		try {
			ois.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
   }
    
	/**
	 * new thread that allow to update the clock o the view
	 * @author donovan
	 */
	private class WaitingPointingsCheck extends Thread{
		
		private PointingMachineController controller;
		@SuppressWarnings("unused")
		private Pointing pointing;
		
		public WaitingPointingsCheck(PointingMachineController controller) {
			this.controller = controller;
		}
		/**
		 * run method : send pointings every 5 minutes
		 */
		@Override
		public void run()
		{
			while(true)
			{
				if(!(model.getPointings().isEmpty())){
					
					for(int i =0; i < model.getPointings().size(); i++){
						pointing = model.getPointings().get(i);
						new Thread(new TCPClientPointing(p, controller)).start();
						model.getPointings().remove(p);
						try
						{
							Thread.sleep(5000);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
				}
				
				try
				{
					Thread.sleep(5000);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}

			}
		}
	}
	
   /** 
    * allow to manage the click on a button 
    */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(view.getCheckBouton()))
			checkBoutonActionPerformed(e);
		else if(e.getSource().equals(view.getSynchronizedBouton()))
			synchronizedBoutonActionPerformed(e);
		
	}
	
	/**
	 * managing the click on the "synchronized button"
	 * @param evt
	 * @throws InterruptedException 
	 */
    private void synchronizedBoutonActionPerformed(java.awt.event.ActionEvent evt){     	
    	ExecutorService executor = Executors.newFixedThreadPool(1);
    	ArrayList<Employee> employees = null;
		try {
			employees = executor.submit(new TCPClientEmployee()).get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		} 
		
		if(employees != null)
			model.setEmployees(employees);
    	    	
    }                                                  

    /**
     * managin the click of the "add" button
     * @param evt
     */
    private void checkBoutonActionPerformed(java.awt.event.ActionEvent evt) { 
    	if(!(view.getComboBox().getSelectedItem() == null || view.getComboBox().getSelectedItem().equals("--employees--") )){
    		
	    	String name = ((String) view.getComboBox().getSelectedItem()).split(" ")[0];
	    	String firstName = ((String) view.getComboBox().getSelectedItem()).split(" ")[1];
	    	e = model.getEmployee(name, firstName);

	    	String heure = view.getjLabel4().getText()+" "+view.getjLabel5().getText();
	
	    	p = new Pointing(e, new RoundedClock(heure));  
	    	new Thread(new TCPClientPointing(p, this)).start();
    	}
    }  
    
    /**
     * get the pointeuse model
     * @return
     */
    public Pointeuse getModel(){
    	return model;
    }

	@Override
	public void windowClosing(WindowEvent e) {
		/** serialize the model */
		try {
			oos = new ObjectOutputStream(new FileOutputStream(serializeFileName));
			oos.writeObject(model);
			oos.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	}
	
	
	/** useless methods from WindowsListenner */
	@Override
	public void windowOpened(WindowEvent e) {}
	@Override
	public void windowClosed(WindowEvent e) {}
	@Override
	public void windowIconified(WindowEvent e) {}
	@Override
	public void windowDeiconified(WindowEvent e) {}
	@Override
	public void windowActivated(WindowEvent e) {}
	@Override
	public void windowDeactivated(WindowEvent e) {	}

}
