package polytech.tours.di.projet.java.app.centrale.model.company;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * manipulate departments
 * @author donovan
 */
public class Department implements Serializable{
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 4830175031235349599L;

	/** Attributes */
    private String name; /** < the department name */
    
    /** Associations */
    private ArrayList<Employee> employees = new ArrayList<Employee>(); /** < the employee list */
    private Manager manager;
    /**
     * the constructor using the name
     * @param name 
     */
    public Department(String name){
    	this.name = name;
    }
    
    /**
     * the constructor using the name
     * @param name 
     */
    public Department(String name, Manager m){
    	this.name = name;
    	this.manager = m;
    }
        
        /**
         * get the department's name
         * @return name
         */
	public String getName() {
		return name;
	}
	
	/**
	 * set the department's name
	 * @param name
	 */
	public void setName(String name){
		this.name = name;
	}

	
	/**
         * get an employee by he's name in the department
         * @param name
         * @param firstname
         * @return the employee
         */
	public Employee getEmployee(String name, String firstname){
		for(Employee e : employees){
			if((e.getName()).equals(name) && (e.getFirstName()).equals(firstname))
				return e;
		}
		return null;	
	}
	
	/**
     * get a manager by he's name in the department
     * @param name
     * @param firstname
     * @return the manager
     */
	public Manager getManager(){

		return manager;
	}
	
	/**
	 * set the manager whose lead the department
	 * @param m : the leader
	 */
	public void setManager(Manager m){
		manager =  m;
	}
	
	/**
	 * add an employee in the department
	 * check if it's a leader and if there is already a leader
	 * @param e the employee to add
	 */
	public void addEmployee(Employee e){
			employees.add(e);
	}

	/**
     * get the employees list
	 * @return the employees
	 */
	public ArrayList<Employee> getEmployees() {
		return employees;
	}
	
	/**
     * get the managers list
	 * @return the managers
	 */
	public ArrayList<Manager> getManagers() {
		ArrayList<Manager> managers = new ArrayList<Manager>();
		for(Employee e : employees){
			if(e.getClass().equals(Manager.class))
				managers.add((Manager) e);
		}
		return managers;
	}


	
    /**
     * the toString method
     * @return String
     */
	public String toString(){
		return name+" managed by : "+getManager();
	}

	
    /**
     * check if an employee exist
     * @param name
     * @param firstName
     * @return true if he exist, false he do not
     */
	public boolean existEmployee(Employee e){
		for(Employee employee : employees){
			if(e.equals(employee))
				return true;
		}
		return false;
	}
	
	/**
	 * return the employee id of the employees's list
	 * @param name
	 * @param firstname
	 * @return
	 */
	public int getEmployeeID(Employee e){
		
		if(!(existEmployee(e)))
			throw new IllegalArgumentException("employee doesn't exist");
		
		for(int i = 0; i<employees.size(); i++){
			if(employees.get(i).equals(e))
				return i;
		}
		return 0;
	
	}

 
}
