package polytech.tours.di.projet.java.app.centrale.model.company;

import java.io.Serializable;

/**
 * manipulate manager that manage the company's departments 
 * @author donovan
 *
 */
public class Manager extends Employee implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6277412248030095114L;
	/** Attributes */
    private String email; /** < the manager's eMail */

    /**
     * the constructor
     * @param nameP
     * @param firstNameP
     * @param arriving
     * @param leaving
     * @param email 
     */
    public Manager(String nameP, String firstNameP, String arriving, String leaving, String email){
    	super(nameP, firstNameP, arriving, leaving);
    	this.email = email;
    }
    
	/**
         * get the email
	 * @return the eMail
	 */
	public String geteMail() {
		return email;
	}

	/**
         * set the email
	 * @param eMail the eMail to set
	 */
	public void seteMail(String eMail) {
		this.email = eMail;
	}

        /**
         * the toString method
         * @return 
         */
	public String toString(){
		return super.toString()+" mail : "+geteMail(); 
	}
} 
