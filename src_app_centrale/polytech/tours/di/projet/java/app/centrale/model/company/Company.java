package polytech.tours.di.projet.java.app.centrale.model.company;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import polytech.tours.di.projet.java.app.centrale.model.time.History;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;
import polytech.tours.di.projet.java.app.centrale.model.time.RoundedClock;
 
/**
 * create a company that contain employees, managers, departments, day
 * the model of the main application
 * @author donovan
 */
public class Company extends Observable implements Serializable{

	private static final long serialVersionUID = 2933635649289653136L;
	/** Associations */
	/** history, list of pointings in pairs */
    private History history = new History();
    /** list of the company's departments */
    private ArrayList<Department> departments =  new ArrayList<Department>();
    /** list of the company's employees */
    private ArrayList<Employee> employees =  new ArrayList<Employee>();


	/**
	 * create a company to make some tests
	 * @throws IOException
	 */
	public void stub() throws IOException{
		
		importCSV("importCSV.csv");
		
		/**
		 * create some pointings...
		 */
		getHistory().addpointing(new Pointing(getEmployee("emp1", "emp1"), new RoundedClock("2018-04-12 08:45")));
		getHistory().addpointing(new Pointing(getEmployee("emp1", "emp1"), new RoundedClock("2018-04-12 17:45")));
		getHistory().addpointing(new Pointing(getEmployee("emp2", "emp2"), new RoundedClock("2018-04-12 08:45")));
		getHistory().addpointing(new Pointing(getEmployee("emp2", "emp2"), new RoundedClock("2018-04-12 18:00")));

	
	}
	
	
    /**
     * import an employees list from a .CSV
     * @param fileName : the name of the file 
     * @throws IOException 
     */
    public void importCSV ( String fileName )
    {
    	/** variables **/
        BufferedReader br = null;
        String[] buffer;
        String line = null;

        /** create buffer reader **/
        try {
			br = new BufferedReader(new FileReader("EmployeesFiles/"+fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        
        /** read the first line of the file **/
		try {
			line = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        /** read the entire file */
        while (line != null) {
        	/** split each file's line */
            buffer = line.split(";");
            if(buffer[6].equals("employee") && !existEmployee(buffer[0], buffer[1]))
            	addEmployee(new Employee(buffer[0], buffer[1], buffer[2], buffer[3]), buffer[5]);
            else if(buffer[6].equals("manager") && !existEmployee(buffer[0], buffer[1]))
            	addEmployee(new Manager(buffer[0], buffer[1], buffer[2], buffer[3],buffer[4]), buffer[5]);
            else if(buffer[6].equals("leader") && !existEmployee(buffer[0], buffer[1])){
            	addEmployee(new Manager(buffer[0], buffer[1], buffer[2], buffer[3],buffer[4]), buffer[5]);
            	getDepartment(buffer[5]).setManager((Manager) getEmployee(buffer[0], buffer[1]));
            }
            
            /** change the line **/
    		try {
    			line = br.readLine();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
        }
 
        /** close buffer reader **/
        try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        setChanged();
        notifyObservers(this);
       
    }
    
    /**
     * export the employees list to a .CSV
     * @param fileName the name of the file that we create
     * @throws IOException 
     */
    public void exportCSV ( String fileName )
    {
    	/** variables **/
    	FileWriter writer = null;
		StringBuilder sb = new StringBuilder();
    	
    	/** new writer */
		try {
			writer = new FileWriter("EmployeesFiles/"+fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		/** check all employees in the company, add it to the file */
    	for(Employee emp : getEmployees()){
    		boolean first = true;
    		
        	List<String> values =new ArrayList<String>();
    		values.add(emp.getName());
    		values.add(emp.getFirstName());
    		values.add(String.valueOf(emp.getArrivingHour().getLdt().toLocalTime().toString()));
    		values.add(String.valueOf(emp.getLeavingHour().getLdt().toLocalTime().toString()));
    		
    		if(emp.getClass().equals(Manager.class))
    			values.add(String.valueOf(((Manager)emp).geteMail()));
    		else
    			values.add(String.valueOf("no email (it's an employee)"));

    		values.add(getDepartment(emp).getName());

            for (String value : values) {
                if (!first) {
                    sb.append(";");
                }
                sb.append(value);
                first = false;
            }
            sb.append(System.getProperty("line.separator"));
    	}
    	try{
		    writer.write(sb.toString());
	        writer.flush();
	        writer.close();
    	}catch(IOException e){
    		e.printStackTrace();
    	}
   
    }
    

    /**
     * import a list of pointing in the company
     * @param importPointingFileName
     */
	public void importPointing(String importPointingFileName) {
    	/** variables **/
        BufferedReader br = null;
        String[] buffer;
        String line = null;
        RoundedClock clock = null;

        /** create buffer reader **/
        try {
			br = new BufferedReader(new FileReader("EmployeesFiles/"+importPointingFileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        
        /** read the first line of the file **/
		try {
			line = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        /** read the entire file */
        while (line != null) {
        	/** split each file's line */
            buffer = line.split(";");
            if(existEmployee(buffer[0], buffer[1])){
                boolean dateValid = true;
                try{
                	clock = new RoundedClock(buffer[2]);
                }catch(Exception e1){
                	dateValid = false;
                }
                if(dateValid)
                	addpointing(new Pointing(getEmployee(buffer[0], buffer[1]), clock));

            }
        
            
            /** change the line **/
    		try {
    			line = br.readLine();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
        
        }
        /** close buffer reader **/
        try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        setChanged();
        notifyObservers(getHistory().getPointings());
	}
    
    /* DEPARTMENTS */
    
    /**
     * get the company's departments
     * @return departments
     */
	public ArrayList<Department> getDepartments() {
   
		return departments;
	}
        	
    
    /**
     * find if a department already exist in the company
     * @param dapartmentName
     * @return true if the department exist, false if it doesn't exist
     */
	public boolean departmentExist(String dapartmentName){
		for(Department d : departments){
			if((d.getName()).equals(dapartmentName))
				return true;
		}
		return false;
	}
	
    /**
     * get a department of the company by it's name
     * @param dapartmentName
     * @return the department if it exist, else return null 
     */
	public Department getDepartment(String dapartmentName){
		
		for(Department d : departments){
			if((d.getName()).equals(dapartmentName))
				return d;
		}
		return null;
	}
	
	
    /**
     * get the department of an employee in the company
     * @param e the employee
     * @return the department if it exist, else return null 
     */
	public Department getDepartment(Employee e){
		
		for(Department d : departments){
			if(d.existEmployee(e))
				return d;
		}
		return null;
	}
	
	
	 /** add a department to the company
     * @param d department to add
     */
     public void addDepartment(Department d){
     	
     	if(!departmentExist(d.getName())){
             departments.add(d);  
             setChanged();
             notifyObservers(this);
     	}
     }
     
     /** remove a department from the company
     * @param d department to remove
     * necesite that there is no more employee and manager in the department
     */
     public void removeDepartment(Department d){
    	 ArrayList<Employee> list = new ArrayList<Employee>(d.getEmployees());
    	 	for(Employee e : list)
    	 		removeEmployee(e.getName(),e.getFirstName());
    	 	
     		departments.remove(d);
            setChanged();
            notifyObservers(this);
     }
     
     /**
      * update a department
      * @param d1
      * @param d2
      */
     public void updateDepartment(String name, String newName, Manager newManager){
    	 if(!departmentExist(newName)){
	    	 getDepartment(name).setManager(newManager);
	    	 getDepartment(name).setName(newName);
	    	 setChanged();
	    	 notifyObservers(this);
    	 }
     }
     

	/* EMPLOYEES */

    /**
     * get the company's employees
     * @return employees
     */
    public ArrayList<Employee> getEmployees() {
    	return employees;
    }
	
    /**
     * get an employee of the company by he's name
     * @param name
     * @param firstName
     * @return e : the employee
     */
	public Employee getEmployee(String name, String firstName){
		for(Employee e : employees){
			if((e.getName()).equals(name) && (e.getFirstName()).equals(firstName))
				return e;
		}
		return null;
	}
	
	/**
	 * return the employee id of the employees's list
	 * @param name
	 * @param firstname
	 * @return
	 */
	public int getEmployeeID(String name, String firstname){
		
		if(!(existEmployee(name, firstname)))
			throw new IllegalArgumentException("employee doesn't exist");
		
		for(int i = 0; i<employees.size(); i++){
			if(employees.get(i).getName().equals(name) && employees.get(i).getFirstName().equals(firstname))
				return i;
		}
		return 0;
	
	}

    /** add an employee or a manager to the company and it's department
    * @param e employee to add
    * @param department department where to add the employee
    */
        public void addEmployee(Employee e, String department){
        	
        	if(!existEmployee(e.getName(), e.getFirstName())){
        		
                employees.add(e);
                
                if(!departmentExist(department))
                    departments.add(new Department(department));

                getDepartment(department).addEmployee(e); 
                
                setChanged();
                notifyObservers(this);

        	}

  
        }
        
    /** remove an employee from the company and it's department
    * @param e employee to remove
    */
    public void removeEmployee(String name, String firstName){
    	/** find the department of the employee */
    	Employee employee = getEmployee(name, firstName);
    	Department department = getDepartment(employee);
    
    	
    	if(department.getManager() != null && department.getManager().equals(employee))
    		department.setManager(null);
    	
    	/** remove it from department */
    	department.getEmployees().remove(department.getEmployee(name, firstName));

    	
    	/** remove employee from the company */
        employees.remove(getEmployee(name, firstName)); 
        
        getHistory().removePointings(employee);
        
        
        setChanged();
        notifyObservers(this);
    }	
    
    /**
     * update an employee
     * @param e1
     * @param e2
     */
    public void updateEmployee(Employee e1, Employee e2){
    	
    	if(e1 == null || e2 == null)
    		throw new IllegalArgumentException("null employee");
    	
    	employees.set(getEmployeeID(e1.getName(), e1.getFirstName()), e2);
    	
    	
    	getDepartment(e1).getEmployees().set(getDepartment(e1).getEmployeeID(e1), e2);
    	
    	if(e1 instanceof Manager && e2 instanceof Employee){
    		if(getDepartment(e1) != null && getDepartment(e1).getManager() != null && getDepartment(e1).getManager().equals(e1))
        		getDepartment(e1).setManager(null);
    	}
    	
    	getHistory().setPointings(e1,e2);
    	
    	setChanged();
    	notifyObservers(this);
    	
    }


    /**
     * check if an employee exist
     * @param name
     * @param firstName
     * @return true if he exist, false he do not
     */
	public boolean existEmployee(String name, String firstName){
		for(Employee e : employees){
			if((e.getName()).equals(name) && (e.getFirstName()).equals(firstName))
				return true;
		}
		return false;
	}

		
	/* MANAGER */
        
    /**
     * get the company's managers
     * @return managers : the list of managers
     */
    public ArrayList<Manager> getManagers(){

    	ArrayList<Manager> managers = new ArrayList<Manager>();
    	
    	for(Employee e : employees){
    		if(e.getClass().equals(Manager.class)){
    			managers.add((Manager) e);
    		}
    	}
    	
    	return managers;
        
    }



	/** get the history of the company
	 * @return the history
	 */
	public History getHistory() {
		return history;
	}

    /**
     * add a check in or out pointing
     * @param p 
     */
    public void addpointing(Pointing p){
    	history.addpointing(p);
    	setChanged();
    	notifyObservers(this);
    }
    
    /**
     * remove a pointing
     * @param p
     */
    public void removePointing(Pointing p){
    	history.removePointing(p);
    	setChanged();
    	notifyObservers(this);
    }
    
    /**
     * update a pointing
     * @param p1
     * @param newClock
     */
    public void updatePointing(Pointing p1, RoundedClock newClock){
    	history.updatePointing(p1, newClock);
    	setChanged();
    	notifyObservers(this);
    }
	
	/**
	* the toString method
	* @return String 
	*/
    @Override
	public String toString(){
		String retour = "EMPLOYEES : \n\n";
		for(Employee e : employees)
			retour += e+"\n";
		retour += "\n\nDEPARTMENTS : \n\n";
		for(Department d : departments)
			retour += d+"\n";
		
		return retour;
	}







}
