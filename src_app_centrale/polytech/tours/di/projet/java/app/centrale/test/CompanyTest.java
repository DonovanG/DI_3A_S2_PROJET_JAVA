/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polytech.tours.di.projet.java.app.centrale.test;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import polytech.tours.di.projet.java.app.centrale.model.company.Company;
import polytech.tours.di.projet.java.app.centrale.model.company.Department;
import polytech.tours.di.projet.java.app.centrale.model.company.Employee;
import polytech.tours.di.projet.java.app.centrale.model.company.Manager;


/**
 * class test for the company model
 * test adding employee, manager, leader, department and remove them
 * @author donovan
 */
public class CompanyTest{
    
	/** attributes */
	private Company company = new Company();
    private Employee employee;
    private Manager manager;
    private Department department;
    
    /**
     * test adding some employee
     */
    @Test
    public void addEmployeeTest(){
        
        employee = company.getEmployee("GUILLOT", "Donovan");
        department = company.getDepartment("IT");
        Assert.assertEquals(employee, null);
        Assert.assertEquals(department, null);

        /** add an employee */
        company.addEmployee(new Employee("GUILLOT","Donovan","08:00","17:00"), "IT");
        employee = company.getEmployee("GUILLOT", "Donovan");
        Assert.assertEquals("GUILLOT", employee.getName());
        Assert.assertEquals("Donovan", employee.getFirstName());
        Assert.assertEquals(8, employee.getArrivingHour().getLdt().getHour());
        Assert.assertEquals(17, employee.getLeavingHour().getLdt().getHour());
        
        /** test creation of the department */
        department = company.getDepartment("IT");
        Assert.assertEquals("IT", department.getName());
        
        /** test adding in department */
        employee = department.getEmployee("GUILLOT", "Donovan");
        Assert.assertEquals("GUILLOT", employee.getName());
        Assert.assertEquals("Donovan", employee.getFirstName());
        Assert.assertEquals(8, employee.getArrivingHour().getLdt().getHour());
        Assert.assertEquals(17, employee.getLeavingHour().getLdt().getHour());
        
        /** test set date with rounding */
        employee.setArrivingHour("08:25");
        Assert.assertEquals(30, employee.getArrivingHour().getLdt().getMinute());
        
        /** remove an employee */
        company.removeEmployee("GUILLOT", "Donovan");
        employee = company.getEmployee("GUILLOT", "Donovan");
        Assert.assertEquals(employee, null);
        employee = department.getEmployee("GUILLOT", "Donovan");
        Assert.assertEquals(employee, null);
        
        /** remove a department */
        company.removeDepartment(department);
        Assert.assertEquals(null,company.getDepartment("IT"));


    }
    
    /**
     * test addind some managers and remove them
     */
    @Test
    public void addManagerTest(){
        manager = (Manager) company.getEmployee("GUILLOT", "Donovan");
        department = company.getDepartment("IT");
        Assert.assertEquals(employee, null);
        Assert.assertEquals(department, null);

        /** add an employee */
        company.addEmployee(new Manager("GUILLOT","Donovan","08:00","17:00","donovan.guillot@etu.univ-tours.fr"), "IT");
        company.getDepartment("IT").setManager((Manager) company.getEmployee("GUILLOT", "Donovan"));
        manager = (Manager) company.getEmployee("GUILLOT", "Donovan");
        Assert.assertEquals("GUILLOT", manager.getName());
        Assert.assertEquals("Donovan", manager.getFirstName());
        Assert.assertEquals(8, manager.getArrivingHour().getLdt().getHour());
        Assert.assertEquals(17, manager.getLeavingHour().getLdt().getHour());
        
        /** test creation of the department */
        department = company.getDepartment("IT");
        Assert.assertEquals("IT", department.getName());
        
        /** test adding in department */
        manager = department.getManager();
        Assert.assertEquals("GUILLOT", manager.getName());
        Assert.assertEquals("Donovan", manager.getFirstName());
        Assert.assertEquals(8, manager.getArrivingHour().getLdt().getHour());
        Assert.assertEquals(17, manager.getLeavingHour().getLdt().getHour());
        
        /** test set date with rounding */
        manager.setArrivingHour("08:25");
        Assert.assertEquals(30, manager.getArrivingHour().getLdt().getMinute());
        
        /** remove an employee */
        company.removeEmployee("GUILLOT", "Donovan");
        manager = (Manager) company.getEmployee("GUILLOT", "Donovan");
        Assert.assertEquals(manager, null);
        manager = department.getManager();
        Assert.assertEquals(null, manager);
    }
    
    /** 
     * test the import method from a .csv 
     */
    @Test
    public void importTest() throws IOException{
    	
		company = new Company();
		company.importCSV("importTestCSV.csv");
		
		/** add employees */
		Assert.assertEquals("emp1", company.getEmployees().get(0).getName());
		Assert.assertEquals("emp2", company.getEmployees().get(1).getName());
		
		/** test Manager vs Employee */
		Assert.assertEquals(Employee.class, company.getEmployees().get(0).getClass());
		Assert.assertEquals(Manager.class, company.getEmployees().get(1).getClass());
		
		
		/**add department */
		Assert.assertEquals("dep1", company.getDepartments().get(0).getName());
		Assert.assertEquals("dep2", company.getDepartments().get(1).getName());
		
		/**add employees to department */
		Assert.assertEquals("emp1", company.getDepartment("dep1").getEmployees().get(0).getName());
		Assert.assertEquals("emp5", company.getDepartment("dep2").getEmployees().get(0).getName());
		
		
		/** time */
		Assert.assertEquals(1111, company.getEmployees().get(0).getArrivingHour().getLdt().getYear());
		Assert.assertEquals(9, company.getEmployees().get(0).getArrivingHour().getLdt().getHour());
		Assert.assertEquals(0, company.getEmployees().get(0).getArrivingHour().getLdt().getMinute());

    	
    }
    
    /**
     * test if there is only one employee with the same name and the name firstname in the company
     * @throws IOException
     */
    @Test
    public void TestDoubleEmployee() throws IOException{
    	
		company = new Company();
		company.addEmployee(new Employee("GUILLOT", "Donovan", "08:15", "17:00"), "IT");
		company.addEmployee(new Employee("GUILLOT", "Donovan", "08:15", "17:00"), "IT");

		ArrayList<Employee> list = new ArrayList<Employee>();
		boolean find = false;
		
    	for(Employee e1 : company.getEmployees()){
    		for(Employee e2 : list){
    			if(e1.getName()==e2.getName() && e1.getFirstName()==e2.getFirstName())
    				find = true;
    		}
    		list.add(e1);
    			
    	}
    	
    	Assert.assertEquals(false, find);
    		
    	
    }
    
    /**
     * test if there is only one department with the same name
     * @throws IOException
     */
    @Test
    public void TestDoubleDepartment() throws IOException{
    	
		company = new Company();
		company.addDepartment(new Department("IT"));
		company.addDepartment(new Department("IT"));


		ArrayList<Department> list = new ArrayList<Department>();
		boolean find = false;
		
    	for(Department d1 : company.getDepartments()){
    		for(Department d2 : list){
    			if(d1.getName()==d2.getName())
    				find = true;
    		}
    		list.add(d1);
    			
    	}
    	
    	Assert.assertEquals(false, find);
    		
    	
    }
    
    /**
     * test if there is only one manager that lead a department
     * @throws IOException
     */
    @Test
    public void TestDoubleManager() throws IOException{
    	
		company = new Company();
		department = new Department("IT");
		Manager manager1 = new Manager("GUILLOT", "Donovan", "08:30", "17:00", "donovan.guillot@etu.univ-tours.fr");
		Manager manager2 = new Manager("GUILLOT", "Donovan", "08:30", "17:00", "donovan.guillot@etu.univ-tours.fr");
		company.addEmployee(manager1, department.getName());
		company.addEmployee(manager2, department.getName());
		
		int compteur = 0;
		
		for(Employee e : company.getDepartment(department.getName()).getEmployees()){
			if(e.getClass().equals(Manager.class))
				compteur++;
		}
		
		Assert.assertEquals(1, compteur);



		
		

		
    		
    	
    }
    


}
    
