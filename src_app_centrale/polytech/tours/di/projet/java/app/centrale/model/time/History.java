package polytech.tours.di.projet.java.app.centrale.model.time;

import java.io.Serializable;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Observable;

import polytech.tours.di.projet.java.app.centrale.model.company.Employee;

/**
 * manipulate a list of pointing in pairs for an employee and a date 
 * @author donovan
 *
 */
public class History extends Observable implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9117112444423874849L;
	/** the list of pointings in pairs */
	private ArrayList<Day> days = new ArrayList<Day>();
	
	
    /**
     * get company's days
     * @return days
     */
	public ArrayList<Day> getDay() {
		return days;
	}
	
    
	/** get the last pointing of an emploee
	 * @param e : the employee
	 * @return Pointing : the last pointing of the employee
	 */
	public Pointing getLastPointing(Employee e){
		if(getPointings(e).size() != 0)
			return getPointings(e).get((getPointings(e).size())-1);
		else
			return null;
	}
	
    /**
     * add a check in or out pointing
     * @param p 
     */
    public void addpointing(Pointing p){
    	 
    	/** get the last pointing of the employee who's linked to the pointing */
    	Pointing lp = getLastPointing(p.getEmployee());
    	
    	/** check if the pointing we want to add is the last */
    	if(lp == null || p.getDate().getLdt().isAfter(lp.getDate().getLdt())){
    		/** check if the pointing already exist */
	    	if(!(existPointing(p))){
	    		/** if the day exist : it's obviously a leaving we want to add */
		    	if(!dayExist(p.getDate(), p.getEmployee())){
		    		days.add(new Day(p, null));
		        	p.getEmployee().updateOvertime(ChronoUnit.MINUTES.between(p.getDate().getLdt().toLocalTime(), p.getEmployee().getArrivingHour().getLdt().toLocalTime())); 
		    	}
		    	/** if the day doesn't exist : it's a new arriving */
		    	else{
		    		/** check if there isn't a leaving */
			    	if(getDay(p.getDate(), p.getEmployee()).getLeaving() == null){
			    		getDay(p.getDate(), p.getEmployee()).setLeaving(p);
			    		p.getEmployee().updateOvertime(-(ChronoUnit.MINUTES.between(p.getDate().getLdt().toLocalTime(), p.getEmployee().getLeavingHour().getLdt().toLocalTime())));
			    	}
		    	}
	
	    	}
    	}
    	/** if the pointing isn't the last but maybe someone forget to check */
    	else if(p.getDate().getLdt().isBefore(lp.getDate().getLdt())){
    		/** check if there is already a day for the pointing we want to add */
    		if(getDay(p) == null){
	    		days.add(new Day(p, null));
	        	p.getEmployee().updateOvertime(ChronoUnit.MINUTES.between(p.getDate().getLdt().toLocalTime(), p.getEmployee().getArrivingHour().getLdt().toLocalTime())); 
    		}
    		/** if there is already a day but the checkout is empty : add the pointing as a leaving */
    		else if(getDay(p).getLeaving() == null && p.getDate().getLdt().isAfter(getDay(p).getArriving().getDate().getLdt())){
    			getDay(p.getDate(), p.getEmployee()).setLeaving(p);
	    		p.getEmployee().updateOvertime(-(ChronoUnit.MINUTES.between(p.getDate().getLdt().toLocalTime(), p.getEmployee().getLeavingHour().getLdt().toLocalTime())));
    		}
    		/**if there is already a day and the checkout is empty but the checkin is after the pointing */
    		else if(getDay(p).getLeaving() == null && p.getDate().getLdt().isBefore(getDay(p).getArriving().getDate().getLdt())){
    			getDay(p.getDate(), p.getEmployee()).setLeaving(getDay(p).getArriving());
    			getDay(p).getArriving().getEmployee().updateOvertime(-(ChronoUnit.MINUTES.between(getDay(p).getArriving().getDate().getLdt().toLocalTime(), getDay(p).getArriving().getEmployee().getArrivingHour().getLdt().toLocalTime())));
	    		p.getEmployee().updateOvertime(-(ChronoUnit.MINUTES.between(getDay(p).getArriving().getDate().getLdt().toLocalTime(), getDay(p).getArriving().getEmployee().getLeavingHour().getLdt().toLocalTime())));
	    		
	    		getDay(p).setArriving(p);
	        	p.getEmployee().updateOvertime(ChronoUnit.MINUTES.between(p.getDate().getLdt().toLocalTime(), p.getEmployee().getArrivingHour().getLdt().toLocalTime())); 

    		}
    	}

    }
    
    /**
     * find if a pointing already exist
     * @param p : the pointing we want to find
     * @return true if the pointing exists, false if it doesn't
     */
	public boolean existPointing(Pointing p){
		for(Pointing pointing : getPointings()){
			if(pointing.equals(p))
				return true;
		}
		return false;
	}
    
	/**
	 * refresh the pointing of an employee if there is an update on him
	 * @param e1 the ancient employee
	 * @param e2 the updated employee
	 */
	public void setPointings(Employee e1, Employee e2){
		for(Pointing p : getPointings(e1))
			p.setEmployee(e2);
		
	}
	
    /**
     * get the entire list of pointings
     * @return pointings, the list
     */
    public ArrayList<Pointing> getPointings(){
    	ArrayList<Pointing> pointings = new ArrayList<Pointing>();
    	for(Day d : days){
    		if(d.getArriving() != null)
        		pointings.add(d.getArriving());
    		if(d.getLeaving() != null)
    			pointings.add(d.getLeaving());
    	}
    	return pointings;
    }
    
    /**
     * get the pointings of an employee
     * @param e the employee
     * @return pointings, the list 
     */
    public ArrayList<Pointing> getPointings(Employee e){
    	ArrayList<Pointing> pointings = new ArrayList<Pointing>();
    	
    	for(Day d : days){
    		if(d.getArriving() !=  null){
        		if(d.getArriving().getEmployee().getName() == e.getName() && d.getArriving().getEmployee().getFirstName() == e .getFirstName())
        			pointings.add(d.getArriving());
    		}
    		if(d.getLeaving() !=  null){
        		if(d.getLeaving().getEmployee().getName() == e.getName() && d.getLeaving().getEmployee().getFirstName() == e .getFirstName())
        			pointings.add(d.getLeaving());
    		}
    		
    	}
    	
    	return pointings;
        
    }
    
    
    /**
     * get a day by a date and an employee
     * @param roundedClock the date where we search
     * @param e the employee
     * @return Day d the day we search
     */
    public Day getDay(RoundedClock roundedClock, Employee e){    	
    	for(Day d : days){
    		if(d.getArriving() != null &&
    				d.getArriving().getDate().getLdt().toLocalDate().equals(roundedClock.getLdt().toLocalDate()) && 
    				d.getArriving().getEmployee().equals(e)){
    			return d;
    		}
    	}
    	return null;
    }
    
    /**
     * get a day by a pointing
     * @param p the pointing
     * @return Day d the day we search
     */
    public Day getDay(Pointing p){    	
    	for(Day d : days){
    		if(d.getArriving() != null &&
    				d.getArriving().getDate().getLdt().toLocalDate().equals(p.getDate().getLdt().toLocalDate()) && 
    				d.getArriving().getEmployee().equals(p.getEmployee())){
    			return d;
    		}
    	}
    	return null;
    }
    
	/**
	 * get a list of pointings for a date
	 * @param date
	 * @return ArrayList<Pointing> list, the list of pointings
	 */
	public ArrayList<Pointing> getPointings(Calendar calendar){
		
		ArrayList<Pointing> list = new ArrayList<Pointing>();
		
		for(Day d : days){
			if(d.getArriving().getDate().equals(calendar)){
				list.add(d.getArriving());
				if(d.getLeaving()!=null)
					list.add(d.getLeaving());
			}
		}
		
		return list;
	}
	
	/**
	 * get a list of pointings for a date
	 * @param date
	 * @return ArrayList<Pointing> list, the list of pointings
	 */
	public ArrayList<Pointing> getPointings(Calendar calendar1, Calendar calendar2){
		
		ArrayList<Pointing> list = new ArrayList<Pointing>();
		
		
		if(calendar1 == null && calendar2 == null){
			for(Day day : days){
				if(day.getArriving() != null)
					list.add(day.getArriving());
				if(day.getLeaving() != null)
					list.add(day.getLeaving());
			}
		}
		else if(calendar1 == null){
			for(Day day : days){
				if(day.getArriving() != null){
					if(calendar2.getTime().after(day.getArriving().getDate().toDate()))
						list.add(day.getArriving());
				}
				if(day.getLeaving() != null)
					if(calendar2.getTime().after(day.getLeaving().getDate().toDate()))
						list.add(day.getLeaving());
			}
		}
		else if(calendar2 == null){
			for(Day day : days){
				if(day.getArriving() != null){
					if(calendar1.getTime().before(day.getArriving().getDate().toDate()))
						list.add(day.getArriving());
				}
				if(day.getLeaving() != null){
					if(calendar1.getTime().before(day.getArriving().getDate().toDate()))
						list.add(day.getLeaving());
				}
			}
		}
		else{
			for(Day day : days){
				if(day.getArriving() != null){
					if(calendar1.getTime().before(day.getArriving().getDate().toDate())
							&& calendar2.getTime().after(day.getArriving().getDate().toDate()))
					list.add(day.getArriving());
				}
				if(day.getLeaving() != null){
					if(calendar1.getTime().before(day.getLeaving().getDate().toDate())
							&& calendar2.getTime().after(day.getLeaving().getDate().toDate()))
					list.add(day.getLeaving());
				}
			}
		}
		
		
		return list;
	}
	
	/**
	 * get a list of pointings by a date and an employee
	 * @param e, the employee
	 * @param calendar, the date
	 * @return ArrayList<Pointing> list, the list of pointings
	 */
	public ArrayList<Pointing> getPointings(Employee e, Calendar calendar){
		
		ArrayList<Pointing> list = new ArrayList<Pointing>();
		
		for(Day d : days){
			if(d.getArriving().getDate().equals(calendar) 
					&& d.getArriving().getEmployee().getName().equals(e.getName())
					&& d.getArriving().getEmployee().getFirstName().equals(e.getFirstName())){
				list.add(d.getArriving());
				if(d.getLeaving()!=null)
					list.add(d.getLeaving());
			}
		}
		
		return list;
	}
	
	/**
	 * get a list of pointings by a date and an employee
	 * @param e, the employee
	 * @param calendar, the date
	 * @return ArrayList<Pointing> list, the list of pointings
	 */
	public ArrayList<Pointing> getPointings(Employee e, Calendar calendar1, Calendar calendar2){
		
		ArrayList<Pointing> list = new ArrayList<Pointing>();
		
		
		if(calendar1 == null && calendar2 == null){
			for(Day day : days){
				if(day.getArriving() != null && day.getArriving().getEmployee().equals(e))
					list.add(day.getArriving());
				if(day.getLeaving() != null && day.getLeaving().getEmployee().equals(e))
					list.add(day.getLeaving());
			}
		}
		else if(calendar1 == null){
			for(Day day : days){
				if(day.getArriving() != null && day.getArriving().getEmployee().equals(e)){
					if(calendar2.getTime().after(day.getArriving().getDate().toDate()))
						list.add(day.getArriving());
				}
				if(day.getLeaving() != null && day.getLeaving().getEmployee().equals(e))
					if(calendar2.getTime().after(day.getLeaving().getDate().toDate()))
						list.add(day.getLeaving());
			}
		}
		else if(calendar2 == null){
			for(Day day : days){
				if(day.getArriving() != null && day.getArriving().getEmployee().equals(e)){
					if(calendar1.getTime().before(day.getArriving().getDate().toDate()))
						list.add(day.getArriving());
				}
				if(day.getLeaving() != null && day.getLeaving().getEmployee().equals(e)){
					if(calendar1.getTime().before(day.getArriving().getDate().toDate()))
						list.add(day.getLeaving());
				}
			}
		}
		else{
			for(Day day : days){
				if(day.getArriving() != null && day.getArriving().getEmployee().equals(e)){
					if(calendar1.getTime().before(day.getArriving().getDate().toDate())
							&& calendar2.getTime().after(day.getArriving().getDate().toDate()))
					list.add(day.getArriving());
				}
				if(day.getLeaving() != null && day.getLeaving().getEmployee().equals(e)){
					if(calendar1.getTime().before(day.getLeaving().getDate().toDate())
							&& calendar2.getTime().after(day.getLeaving().getDate().toDate()))
					list.add(day.getLeaving());
				}
			}
		}
		
		
		return list;
	}

	
	/**
	 * remove the pointings of an employee
	 * @param e
	 */
	public void removePointings(Employee e){
			days.removeAll(getDays(e));
	}


	/**
	 * return the day's list of an employee
	 * @param e
	 * @return
	 */
	private ArrayList<Day> getDays(Employee e) {
		ArrayList<Day> list = new ArrayList<Day>();
		for(Day d : days){
			if(d.getArriving()!=null){
				if(d.getArriving().getEmployee().equals(e))
					list.add(d);
			}
			else if(d.getLeaving()!=null){
				if(d.getLeaving().getEmployee().equals(e))
					list.add(d);
			}
		}
		return list;
	}


	/**
     * find if a day exist 
     * @param roundedClock the date of the day
     * @param e the employee for who we search the day
     * @return true if the day exist, false if it doesn't
     */
    public boolean dayExist(RoundedClock roundedClock, Employee e){
    	for(Day d : days){
    		if((d.getArriving() != null && d.getArriving().getDate().getLdt().toLocalDate().equals(roundedClock.getLdt().toLocalDate()) && d.getArriving().getEmployee().equals(e)) ||
    			(d.getLeaving() != null && d.getLeaving().getDate().getLdt().toLocalDate().equals(roundedClock.getLdt().toLocalDate()) && d.getLeaving().getEmployee().equals(e)))
    			return true;
    	}
    	return false;
    }
    
    /**
     * remove a day in the day list
     * @param d, the day to remove
     */
    public void removeDay(Day d){
    	days.remove(d);
    }

    /**
     * remove a single pointing from the history
     * @param pointing
     */
	public void removePointing(Pointing pointing) {
		
		if(pointing == null)
			throw new IllegalArgumentException("pointing null");
		
		Day day = getDay(pointing.getDate(), pointing.getEmployee());
		
		if(day != null){
			/** if we delete an arriving we put the leaving to it's place */
			if(day.isArriving(pointing)){
				/** update employee's overtime */
	    		pointing.getEmployee().updateOvertime(-(ChronoUnit.MINUTES.between(pointing.getDate().getLdt().toLocalTime(), pointing.getEmployee().getArrivingHour().getLdt().toLocalTime())));
				day.setArriving(day.getLeaving());
				/** delete the leaving */
				day.setLeaving(null);

			}
			else{
				/** update employee's overtime */
	    		pointing.getEmployee().updateOvertime(-(ChronoUnit.MINUTES.between(pointing.getDate().getLdt().toLocalTime(), pointing.getEmployee().getLeavingHour().getLdt().toLocalTime())));
				/** delete the leaving */
				day.setLeaving(null);
			}

			
			/** if there are nore more pointings in the day, delete it */
			if(day.getArriving() == null && day.getLeaving() == null)
				days.remove(day);
		}
		
		/** notify the view */
		setChanged();
		notifyObservers(getPointings());
		
	}
	
	/**
	 * update a pointing of the history
	 * @param p1
	 * @param p2
	 */
	public void updatePointing(Pointing p1, RoundedClock newClock){
		for(Day d : days){
			if(d.getArriving() != null && d.getArriving().equals(p1)){
	    		p1.getEmployee().updateOvertime(ChronoUnit.MINUTES.between(newClock.getLdt().toLocalTime(), p1.getDate().getLdt().toLocalTime()));
				d.getArriving().setDate(newClock);
			}
			else if(d.getLeaving() != null && d.getLeaving().equals(p1)){
	    		p1.getEmployee().updateOvertime(-(ChronoUnit.MINUTES.between(newClock.getLdt().toLocalTime(), p1.getDate().getLdt().toLocalTime())));
				d.getLeaving().setDate(newClock);
			}

		}
		
		setChanged();
		notifyObservers(getPointings());
	}

}
