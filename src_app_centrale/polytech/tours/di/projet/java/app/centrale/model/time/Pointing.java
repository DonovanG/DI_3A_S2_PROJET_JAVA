package polytech.tours.di.projet.java.app.centrale.model.time;

import java.io.Serializable;

import polytech.tours.di.projet.java.app.centrale.model.company.Employee;

/**
 * manipulate company's pointings
 * @author donovan
 *
 */
public class Pointing implements Serializable, Comparable<Pointing>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6400553516421755518L;
	/** Associations */
    private Employee employee; /** < the employee linked to the pointing */
    private RoundedClock roundedClock; /** < the hour of the pointing */
    
    /**
     * the constructor
     * @param employee
     * @param roundedClock 
     */
    public Pointing(Employee employee, RoundedClock roundedClock){
    	this.employee = employee;
    	this.roundedClock = roundedClock;
    }
    
	/**
         * get the hour
	 * @return the roundedClock
	 */
	public RoundedClock getDate() {
		return roundedClock;
	}
	
	/**
	 * set the date
	 * @param date
	 */
	public void setDate(RoundedClock date){
		roundedClock = date;		
	}

	/**
     * get the employee
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}
	
	/**
	 * set the employee
	 * @param e
	 */
	public void setEmployee(Employee e){
		employee = e;
	}
	
	/**
	 * the toString method
	 */
	public String toString(){
		return employee.toString()+" "+roundedClock.toString();
	}

	/**
	 * the equals method
	 * @param p
	 * @return
	 */
	public boolean equals(Pointing p){
		if(p.getEmployee().equals(employee) && p.getDate().equals(roundedClock))
			return true;
		return false;
	}

	/**
	 * surcharge of compareTo method that allow to sort a list of Pointing
	 * @param Pointing p : the pointing wa want to compare with
	 * @return 0 for equality, 1 if p is before, -1 if p is after
	 */
	@Override
	public int compareTo(Pointing p) {
		
		if(p.getDate().getLdt().isBefore(roundedClock.getLdt()))
			return 1;
		else if(p.getDate().getLdt().isAfter(roundedClock.getLdt()))
			return -1;
		else
			return 0;
	}

}
