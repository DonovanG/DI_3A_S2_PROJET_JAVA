package polytech.tours.di.projet.java.app.centrale.model.company;

import java.io.Serializable;

import polytech.tours.di.projet.java.app.centrale.model.time.RoundedClock;

/**
 * manipule an employee of the company
 * @author donovan
 *
 */
public class Employee implements Serializable{
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 8345018668787304342L;
	/** Attributes */
    private String name; /** < the employee name */
    private String firstName; /** < the employee first name */
    private RoundedClock arrivingHour = null; /** < the arriving hour */
    private RoundedClock leavingHour = null; /** < the leaving hour */
    private long overtime = 0; /** < the overtime total */

    /**
     * constructor
     * @param nameP
     * @param firstNameP
     * @param arriving
     * @param leaving 
     */
    public Employee(String nameP, String firstNameP, String arriving, String leaving){
    	name = nameP;
    	firstName = firstNameP;
    	arrivingHour = new RoundedClock("1111-11-11 "+arriving);
    	leavingHour = new RoundedClock("1111-11-11 "+leaving);
    	overtime = 0;
    }
    	
	/**
         * get the name
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
         * set the name         
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
         * get the first name
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
         * set the first name
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
         * get the overtime
	 * @return the overtime
	 */
	public long getOvertime() {
		return overtime;
	}
	/**
         * add overtime to the total
	 * @param overtime the overtime to set
	 */
	public void updateOvertime(long overtime) {
		this.overtime += overtime;
	}
	/**
         * put overtime to zero
         */
	public void refreshOvertime(){
		overtime = 0;
	}
	/**
         * get arriving hour
	 * @return the arrivingHour
	 */
	public RoundedClock getArrivingHour() {
		return arrivingHour;
	}
	/**
         * set arriving hour
	 * @param arrivingHour the arrivingHour to set
	 */
	public void setArrivingHour(String arrivingHour) {
		this.arrivingHour = new RoundedClock(arrivingHour);
	}
	/**
         * get leaving hour
	 * @return the leavingHour
	 */
	public RoundedClock getLeavingHour() {
		return leavingHour;
	}
	/**
         * set leaving hour
	 * @param leavingHour the leavingHour to set
	 */
	public void setLeavingHour(String leavingHour) {
		this.leavingHour = new RoundedClock(leavingHour);
	}
	/**
     * the toString method
     * @return String
     */
	public String toString(){
		return name+" "+firstName+
				" arrives at : "+arrivingHour.getLdt().getHour()+":"+arrivingHour.getLdt().getMinute()+
				" leaves at : "+leavingHour.getLdt().getHour()+":"+leavingHour.getLdt().getMinute()+" overtime : "+overtime+"min";
	}

	/**
	 * the equals method
	 * @param e
	 * @return
	 */
	public boolean equals(Employee e){
		if(name.equals(e.getName()) && firstName.equals(e.getFirstName()))
			return true;
		else 
			return false;
	}
    
}

