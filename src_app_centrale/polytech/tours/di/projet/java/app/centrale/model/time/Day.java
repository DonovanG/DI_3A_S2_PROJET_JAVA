package polytech.tours.di.projet.java.app.centrale.model.time;

import java.io.Serializable;

/**
 * create a duo of pointings for one employee
 * @author donovan
 */
public class Day implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8352267758203201555L;
	/** Associations */
    private Pointing arriving; /** < the check in */
    private Pointing leaving; /** < the check out */
    
    /**
     * the constructor
     * @param arriving
     * @param leaving 
     */
    public Day(Pointing arriving, Pointing leaving){
    	this.arriving = arriving;
    	this.leaving = leaving;
    }
    
    /**
     * get the check in
     * @return arriving
     */
    public Pointing getArriving() {
            return arriving;
    }
    
    /**
     * set the check in
     * @param arriving 
     */
    public void setArriving(Pointing arriving) {
            this.arriving = arriving;
    }
    
    /**
     * get the check out
     * @return leaving
     */
    public Pointing getLeaving() {
            return leaving;
    }
    
    /**
     * set the check out
     * @param leaving 
     */
    public void setLeaving(Pointing leaving) {
            this.leaving = leaving;
    }
    
    /**
     * the toString method
     */
    public String toString(){
    	return "arriving : "+arriving+" leaving : "+leaving;
    }
    
    /**
     * check if a pointing is an arriving or a leaving
     * @param p
     * @return
     */
    public boolean isArriving(Pointing p){
    	if(arriving != null & arriving.equals(p))
    		return true;
    	return false;
    }

}
 
