package polytech.tours.di.projet.java.app.centrale.controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JOptionPane;

import polytech.tours.di.projet.java.app.centrale.model.TCP.TCPServerEmployee;
import polytech.tours.di.projet.java.app.centrale.model.TCP.TCPServerPointing;
import polytech.tours.di.projet.java.app.centrale.model.company.Company;
import polytech.tours.di.projet.java.app.centrale.model.company.ConfigV2;
import polytech.tours.di.projet.java.app.centrale.model.company.Department;
import polytech.tours.di.projet.java.app.centrale.model.company.Employee;
import polytech.tours.di.projet.java.app.centrale.model.company.Manager;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;
import polytech.tours.di.projet.java.app.centrale.model.time.RoundedClock;
import polytech.tours.di.projet.java.app.centrale.vue.MainWindow;

/**
 * the controller of the main application
 * @author donovan
 *
 */
public class CompanyController implements ActionListener, WindowListener{

	
	/** the company model */
	private Company company;
	/** the view */
	private MainWindow view;
	
	/** attributes */
	private Employee employee1;
	private Employee employee2;
	private String EmployeeName;
	private String EmployeeFirstName;
	private String arriving;
	private String leaving;
	private String DptName;
	private String email;
	private Pointing pointing;
	private RoundedClock clock;
	@SuppressWarnings("unused")
	private RoundedClock clock2;
	
	/**Threads */
	Thread threadEmployee;
	Thread threadPointing;
    
	/**initialize objects for the serialization */
	ObjectOutputStream oos = null;
	ObjectInputStream ois = null;
	
	/** import & export file name **/
	String importFileName = "importCSV.csv";
	String exportFileName = "exportCSV.csv";
	String importPointingFileName = "importPointing.csv";

	/** serialize file name**/
	String serializeFileName = "company.ser";


	/**
	 * the constructor
	 * @param company the model
	 */
	public CompanyController() {
        
		ConfigV2.update();


    	try {
	    	ois = new ObjectInputStream(new FileInputStream(serializeFileName));
			this.company = (Company) ois.readObject();


		} catch (IOException e1) {
			this.company = new Company();
		} catch (ClassNotFoundException e) {
			this.company = new Company();
		}
    	    	    	
		
		/** create the view */
		this.view = new MainWindow(this);
		
		/** fill the view tables with the company data */
		this.view.populateTables(company);
		this.view.populateComboBox(company);
		
		/** add the view as an observer in the model Company */
        this.company.addObserver(view);
        this.company.getHistory().addObserver(view);
        
        /** start a server to send employee's list */
		threadEmployee = new Thread(new TCPServerEmployee(company.getEmployees()));
		threadEmployee.start();

		/** start a server to receive poitings */
		threadPointing = new Thread(new TCPServerPointing(this));
		threadPointing.start();
		
		try {
			ois.close();
		} catch (IOException e) {
			System.out.println("ois was never opened");
		}
		
	}
	

	/** managing the buttons actions of the IHM
	 * @param ActionEvent e : the event that provoke the action
	*/
	@Override
	public void actionPerformed(ActionEvent e) {
		/** add buttons */
		if(e.getSource().equals(view.getEmployeeDialog().getBtnAdd()))
			addEmployeeActionPerformed(e);
		else if(view.getDptDialog() != null && e.getSource().equals(view.getDptDialog().getBtnAdd()))
			addDepartmentActionPerformed(e);
		else if(e.getSource().equals(view.getCheckAddDialog().getBtnAdd()))
			addPointingActionPerformed(e);
		/** search buttons */
		else if(e.getSource().equals(view.getBtnCheckPanelSrch()))
			getEmployeePointingsActionPerformed(e);
		else if(e.getSource().equals(view.getBtnEmpPanelSrch()))
			getSearchEmployeeActionPerformed(e);
		else if(e.getSource().equals(view.getBtnDptPanelSrch()))
			getSearchDptActionPerformed(e);
		/** remove buttons */
		else if(e.getSource().equals(view.getBtnCheckPanRmv()))
			getRemoveCheckActionPerformed(e);
		else if(e.getSource().equals(view.getEmpPanelRmv()))
			getRemoveEmployeeActionPerformed(e);
		else if(e.getSource().equals(view.getBtnDptPanelRmv()))
			getRemoveDptActionPerformed(e);
		/** modify buttons */
		else if(e.getSource().equals(view.getEmployeeModifyDialog().getBtnUpdate()))
				getModifyEmployeeActionPerformed(e);
		else if(view.getDptModifyDialog() != null && e.getSource().equals(view.getDptModifyDialog().getBtnUpdate()))
			getModifyDptActionPerformed(e);
		else if(e.getSource().equals(view.getCheckModifyDialog().getBtnUpdate()))
			modifyPointingActionPerformed(e);
		/** import & export menu item **/
		else if(e.getSource().equals(view.getMntmImportCsv()))
			company.importCSV(importFileName);
		else if(e.getSource().equals(view.getMntmExportCsv()))
			company.exportCSV(exportFileName);
		else if(e.getSource().equals(view.getMntmImportPointing()))
			company.importPointing(importPointingFileName);
		/** config buttons */
		else if(e.getSource().equals(view.getBtnIpUpdate()))
			updateIpActionPerformed(e);
		else if(e.getSource().equals(view.getBtnPortUpdate()))
			updatePortEmployeetActionPerformed(e);
		else if(e.getSource().equals(view.getBtnPortUpdate()))
			updatePortPointingActionPerformed(e);
		else if(e.getSource().equals(view.getBtnIncidenceUpdate()))
			updateIncidenceActionPerformed(e);


	}

	private void updateIncidenceActionPerformed(ActionEvent e) {
		String incidence = view.getTxtFldIncidence().getText();
		if(!(incidence.equals(""))){
			ConfigV2.setAccidentThreshold(Integer.parseInt(incidence));
			view.getTxtFldIncidence().setText(incidence);
		}	
		ConfigV2.save();

		
	}


	/**
	 * action by clicking on update pointing port 
	 * @param e
	 */
	private void updatePortPointingActionPerformed(ActionEvent e) {
		
		String port = view.getTxtFieldPortPointing().getText();
		if(!(port.equals(""))){
			ConfigV2.setPortServerPointing(Integer.parseInt(port));
			view.getTxtFieldPortPointing().setText(port);
		}
		ConfigV2.save();

	}

	/**
	 * action by clicking on update employee port 
	 * @param e
	 */
	private void updatePortEmployeetActionPerformed(ActionEvent e) {
		
		String port = view.getTxtFielPortEmp().getText();
		if(!(port.equals(""))){
			ConfigV2.setPortServerEmployee(Integer.parseInt(port));
			view.getTxtFielPortEmp().setText(port);
		}
		ConfigV2.save();


	}

	/**
	 * action by clicking on update IP
	 * @param e
	 */
	private void updateIpActionPerformed(ActionEvent e) {
		String ip = view.getTxtFielIP().getText();
		
		
		if(!(ip.equals(""))){
			ConfigV2.setIpAdress(ip);
			view.getTxtFielIP().setText(ip);
		}
		ConfigV2.save();

	
	}


	/**
	 * action by clicking on update in the check in/out panel
	 * @param e
	 */
	private void modifyPointingActionPerformed(ActionEvent e) {
	
		if(view.getTblPointing().getSelectedRow() != -1){
			/** get the text fields from the pointing dialog */
			EmployeeName = (String) view.getTblPointing().getValueAt(view.getTblPointing().getSelectedRow(), 0) ;
			EmployeeFirstName = (String) view.getTblPointing().getValueAt(view.getTblPointing().getSelectedRow(), 1 );
			leaving = (String) view.getTblPointing().getValueAt(view.getTblPointing().getSelectedRow(), 2);
			arriving = view.getCheckModifyDialog().getjTextField1().getText();
			
			
			boolean dateValid = true;
			try{
				clock = new RoundedClock(arriving);
				clock2 = new RoundedClock(leaving);
			}catch(Exception e1){
				dateValid = false;
			}
			
			/** if the user enter something we can update the pointing */
			if(dateValid && !(EmployeeName.equals("")) && !(EmployeeFirstName.equals(""))){
					try{
						company.updatePointing(new Pointing(company.getEmployee(EmployeeName, EmployeeFirstName), new RoundedClock(leaving)), new RoundedClock(arriving));
					}catch(Exception e1){
						System.out.println("error updatePointing");
					}
					view.getCheckModifyDialog().setVisible(false);
			} else {
				new JOptionPane();
				JOptionPane.showMessageDialog(null, "Invalid pointing, try again", "Input Error", JOptionPane.ERROR_MESSAGE );
			}
		}
	}


	/**
	 * action by clicking on the add button in the check in/out panel
	 * @param e
	 */
	private void addPointingActionPerformed(ActionEvent e) {
		/** get the text fields from the pointing dialog */
		arriving = view.getCheckAddDialog().getjTextField1().getText();
		
		if(view.getCheckAddDialog().getCbEmployee().getSelectedItem() != null){
			EmployeeName = ((String) view.getCheckAddDialog().getCbEmployee().getSelectedItem()).split(" ")[0];
			EmployeeFirstName = ((String) view.getCheckAddDialog().getCbEmployee().getSelectedItem()).split(" ")[1];
					
			boolean dateValid = true;
			try{
				clock = new RoundedClock(arriving);
			}catch(Exception e1){
				dateValid = false;
			}
			
			/** if the user enter something we can add the pointing */
			if(dateValid){
				company.addpointing(new Pointing(company.getEmployee(EmployeeName, EmployeeFirstName), clock));
				view.getCheckAddDialog().setVisible(false);
			}else {
				new JOptionPane();
				JOptionPane.showMessageDialog(null, "Invalid pointing, try again", "Input Error", JOptionPane.ERROR_MESSAGE );
			}
		}
	}

	/**
	 * action by clicking on the remove button in the check in/out panel
	 * @param e
	 */
	private void getRemoveCheckActionPerformed(ActionEvent e) {
		/** check if something is selected in the pointing table */
		if(view.getTblPointing().getSelectedRow() != -1){
			/** get the text fields from the pointing dialog */
			EmployeeName = (String) view.getTblPointing().getValueAt(view.getTblPointing().getSelectedRow(), 0 ) ;
			EmployeeFirstName = (String) view.getTblPointing().getValueAt(view.getTblPointing().getSelectedRow(), 1);
			arriving = (String) view.getTblPointing().getValueAt(view.getTblPointing().getSelectedRow(), 2);
			/** remove the pointing from the company */
			pointing = new Pointing(company.getEmployee(EmployeeName, EmployeeFirstName), new RoundedClock(arriving));
			company.removePointing(pointing);
		}
	}


	/**
	 * action released by clicking on update in the modify dialog of a department
	 * @param e
	 */
	private void getModifyDptActionPerformed(ActionEvent e) {
		/** check if a department is selected */
    	if(view .getTblDpt().getSelectedRow() != -1)
        	DptName = (String) view .getTblDpt().getValueAt(view.getTblDpt().getSelectedRow(), 0);
    	if (!(view.getCbDptPanel().getSelectedItem().equals("--departments--"))){
    		DptName = (String) view.getCbDptPanel().getSelectedItem();
    	}
    	
    	if(view.getDptModifyDialog().getCbManager().getSelectedItem() != null){
			/** get the text fields from the department dialog */
	    	EmployeeName = ((String) view.getDptModifyDialog().getCbManager().getSelectedItem()).split(" ")[0];
	    	EmployeeFirstName = ((String) view.getDptModifyDialog().getCbManager().getSelectedItem()).split(" ")[1];
	    	if(!(view.getDptModifyDialog().getjTextField1().getText().equals("")))
	    		/**update the department name and manager */
	    		company.updateDepartment(DptName, view.getDptModifyDialog().getjTextField1().getText(), (Manager) company.getEmployee(EmployeeName, EmployeeFirstName));
    	}
	    else
    		company.updateDepartment(DptName, view.getDptModifyDialog().getjTextField1().getText(), null);

    		
        view.getDptModifyDialog().setVisible(false);
    	
	}

	/**
	 * action released by clicking on update in the modify dialog of an employee
	 * @param e
	 */
	private void getModifyEmployeeActionPerformed(ActionEvent e){
		/** check if an employee is selected */
    	if(view .getTblEmployee().getSelectedRow() != -1){
			/** get the text fields from the employee table */
        	EmployeeName = (String) view .getTblEmployee().getValueAt(view.getTblEmployee().getSelectedRow(), 0);
        	EmployeeFirstName = (String) view .getTblEmployee().getValueAt(view.getTblEmployee().getSelectedRow(), 1);
    	}
    	else if (!(view.getCbEmpPanel().getSelectedItem().equals("--employees--"))){
			/** get the text fields from the employee dialog */
    		EmployeeName = ((String) view.getCbEmpPanel().getSelectedItem()).split(" ")[0];
    		EmployeeFirstName = ((String) view.getCbEmpPanel().getSelectedItem()).split(" ")[1];
    	}

    	/** get the employee to update */
		employee1 = company.getEmployee(EmployeeName, EmployeeFirstName);
		
		/** get fields of dialog frame */
    	EmployeeName = view.getEmployeeModifyDialog().getTxtFldName().getText();
    	EmployeeFirstName = view.getEmployeeModifyDialog().getTxtFldFName().getText();
    	arriving = view.getEmployeeModifyDialog().getTxtFldArriving().getText();
    	leaving = view.getEmployeeModifyDialog().getTxtFldLeaving().getText();
    	DptName = view.getEmployeeModifyDialog().getTxtFldDpt().getText();
    	email = view.getEmployeeModifyDialog().getTxtFldMail().getText();
    	
		boolean dateValid = true;
		try{
			clock = new RoundedClock(arriving);
			clock2 = new RoundedClock(leaving);
		}catch(Exception e1){
			dateValid = false;
		}
		
    	/** if all the fields are selected, add an employee */
    	if(!(EmployeeName.equals("")) && !(EmployeeFirstName.equals("")) && !(DptName.equals("")) && dateValid){
    		
	    	if(employee1.getClass().equals(Employee.class)){
	    		/** if it's an employee and we want a manager, do the update */
	    		if(view.getEmployeeModifyDialog().getChckBxManager().isSelected()){
	        		employee2 = new Manager(EmployeeName, EmployeeFirstName, arriving, leaving,email);
	    		}
	    		else{
	        		employee2 = new Employee(EmployeeName, EmployeeFirstName, arriving, leaving);
	    		}
	    	}
	    	else if(employee1.getClass().equals(Manager.class)){
	    		if(view.getEmployeeModifyDialog().getChckBxManager().isSelected()){
	        		employee2 = new Manager(EmployeeName, EmployeeFirstName, arriving, leaving,email);
	    		}
	    		else{
	        		employee2 = new Employee(EmployeeName, EmployeeFirstName, arriving, leaving);
	    		}
	    	}
	
	
	    	/** if all the fields are selected, add an employee */
	    	if(!EmployeeName.equals("") && !EmployeeFirstName.equals("") && !arriving.equals("") && !leaving.equals("")){
	    		try{
		    		company.updateEmployee(employee1, employee2);
	    		}catch(IllegalArgumentException e1){
	    			System.out.println(e1.getMessage());
	    		}
	    	}
	    	
	    	
	    	/** close the dialog */
	    	view.getEmployeeModifyDialog().setVisible(false);
    	}else {
			new JOptionPane();
			JOptionPane.showMessageDialog(null, "Invalid fields, try again", "Input Error", JOptionPane.ERROR_MESSAGE );
		}
    }


	/**
	 * action used by clicking on the "remove" button on the department panel
	 * @param e, the event that provoke the action
	 */
	private void getRemoveDptActionPerformed(ActionEvent e) {
		if(view.getTblDpt().getSelectedRow() >= 0){
			DptName = (String) view .getTblDpt().getValueAt(view.getTblDpt().getSelectedRow(), 0);		
			company.removeDepartment(company.getDepartment(DptName));
		}
	}

	/**
	 * action used by clicking on the "remove" button on the employee panel
	 * @param e, the event that provoke the action
	 */
	private void getRemoveEmployeeActionPerformed(ActionEvent e) {
			if(view.getTblEmployee().getSelectedRow() >= 0){
				EmployeeName = (String) view .getTblEmployee().getValueAt(view.getTblEmployee().getSelectedRow(), 0);
				EmployeeFirstName = (String) view .getTblEmployee().getValueAt(view.getTblEmployee().getSelectedRow(), 1);
			
				company.removeEmployee(EmployeeName, EmployeeFirstName);
			}

	}


	/**
	 * action used by clicking on the "search" button on the department panel
	 * @param e, the event that provoke the action
	 */
	private void getSearchDptActionPerformed(ActionEvent e) {
		ArrayList<Department> list = new ArrayList<Department>();
		String buffer = (String) view.getCbDptPanel().getSelectedItem();
		
		/** if no department is selected, print all the departments of the company */
		if(buffer.equals("--departments--"))
			list = company.getDepartments();
		else
			/** else find the department to print */
			list.add(company.getDepartment((String) view.getCbDptPanel().getSelectedItem()));
		
		view.populateDepartmentTable(list);
	}


	/**
	 * action used by clicking on the "search" button on the employee panel
	 * @param e, the event that provoke the action
	 */
	private void getSearchEmployeeActionPerformed(ActionEvent e) {
		ArrayList<Employee> list = new ArrayList<Employee>();
		String[] buffer = ((String) view.getCbEmpPanel().getSelectedItem()).split(" ");
		
		/** if no employee is selected, print all the employees of the company */
		if(buffer[0].equals("--employees--"))
			list = company.getEmployees();
		else
			/** else find the employee to print */
			list.add(company.getEmployee(buffer[0], buffer[1]));
		
		view.populateEmployeeTable(list);
	}

	/**
	 * action used by clicking on the "search" button on the pointing panel
	 * @param e, the event that provoke the action
	 */
	private void getEmployeePointingsActionPerformed(ActionEvent e){
		ArrayList<Pointing> list = new ArrayList<Pointing>();
		/** stock the employee's names */
		String[] buffer = ((String) view.getCbEmpChckPanel().getSelectedItem()).split(" ");
		/** the date selected */
		Calendar calendar1 = view.getDateChooserFrom().getCalendar();
		Calendar calendar2 = view.getDateChooserTo().getCalendar();		
		/** if we are searching for the global list */
		if(buffer[0].equals("--company's")){
				list = company.getHistory().getPointings(calendar1, calendar2);
		}
		/** if we are searching for a specific employee */
		else{
			employee1 = company.getEmployee(buffer[0], buffer[1]);

			list = company.getHistory().getPointings(employee1, calendar1, calendar2);
		}
		
		view.populatePointingTable(list);
	}
	
	/**
	 * action used by clicking on the "add" button on the employee frame
	 * @param evt, the event that provoke the action
	 */
    private void addEmployeeActionPerformed(java.awt.event.ActionEvent evt) {                                         
    	/** get fields */
    	EmployeeName = view.getEmployeeDialog().getTxtFldName().getText();
    	EmployeeFirstName = view.getEmployeeDialog().getTxtFldFName().getText();
    	arriving = view.getEmployeeDialog().getTxtFldArriving().getText();
    	leaving = view.getEmployeeDialog().getTxtFldLeaving().getText();
    	DptName = view.getEmployeeDialog().getTxtFldDpt().getText();
    	email = view.getEmployeeDialog().getTxtFLdMail().getText();
    	
		boolean dateValid = true;
		try{
			clock = new RoundedClock(arriving);
			clock2 = new RoundedClock(leaving);
		}catch(Exception e1){
			dateValid = false;
		}
		
    	/** if all the fields are selected, add an employee */
    	if(!(EmployeeName.equals("")) && !(EmployeeFirstName.equals("")) && !(DptName.equals("")) && dateValid){
    		if(view.getEmployeeDialog().getCbManager().isSelected()){
    			if(!(email.equals(""))){
	    			try{
	        			company.addEmployee(new Manager(EmployeeName, EmployeeFirstName, arriving, leaving,email), DptName);
	    			}catch(Exception e1){
	    				System.out.println("error add Manager");
	    			}
	    	    	
	    	    	/** close the dialog */
	    	    	view.getEmployeeDialog().setVisible(false);
    			}else {
    				new JOptionPane();
    				JOptionPane.showMessageDialog(null, "Enter a valid email", "Input Error", JOptionPane.ERROR_MESSAGE );
    			}
    		}
    		else{
    			try{
        			company.addEmployee(new Employee(EmployeeName, EmployeeFirstName, arriving, leaving), DptName);
    			}catch(Exception e2){
    				System.out.println("error add employee");
    			}
    	    	
    	    	/** close the dialog */
    	    	view.getEmployeeDialog().setVisible(false);
    		}

    	}else {
			new JOptionPane();
			JOptionPane.showMessageDialog(null, "Invalid fields, try again", "Input Error", JOptionPane.ERROR_MESSAGE );
		}

    } 
    
	/**
	 * action used by clicking on the "add" in the department frame 
	 * @param evt
	 */
    private void addDepartmentActionPerformed(java.awt.event.ActionEvent evt) {                                         
    	/** get the text field and check if it isn't empty */
    	DptName = view.getDptDialog().getTxtFldDpt().getText();

    	
    	if(!DptName.equals(""))
    		company.addDepartment(new Department(DptName));

    	view.getDptDialog().setVisible(false);

    } 
                                           
    /**
     * company model getter
     * @return company
     */
    public Company getCompany(){
    	return company;
    }


    /**
     * serialize the company model when we close the window
     */
	@Override
	public void windowClosing(WindowEvent e) {
		/** serialize the model */
		try {
			oos = new ObjectOutputStream(new FileOutputStream(serializeFileName));
			oos.writeObject(this.company);
			oos.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		ConfigV2.save();
		
	}


	/** useless method */
	@Override
	public void windowClosed(WindowEvent e) {}
	@Override
	public void windowIconified(WindowEvent e) {}
	@Override
	public void windowDeiconified(WindowEvent e) {}
	@Override
	public void windowActivated(WindowEvent e) {}
	@Override
	public void windowDeactivated(WindowEvent e) {}
	@Override
	public void windowOpened(WindowEvent e) {}

}