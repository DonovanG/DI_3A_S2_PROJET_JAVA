package polytech.tours.di.projet.java.app.centrale.model.company;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class ConfigV2 {
	
	/** ip adress for TCP */
	private static String ipAdress="localhost";
	/** port for sending and receiving employees by TCP */
	private static int portServerEmployee=8081;
	/** port for sending and receiving pointings by TCP */
	private static int portServerPointing=8085;
	/** allow to know if a pointing is to late to be "normal", warn from incidents */
	private static int accidentThreshold=30;
	
	/**
	 * get the ip adress
	 * @return String ipAdress
	 */
	public static String getIpAdress() {
		return ipAdress;
	}
	
	/**
	 * set the ip adress
	 * @param ipAdress
	 */
	public static void setIpAdress(String ipAdress) {
		ConfigV2.ipAdress = ipAdress;
	}
	
	/**
	 * get the port for send/receive Employees
	 * @return int : the port
	 */
	public static int getPortServerEmployee() {
		return portServerEmployee;
	}
	
	/**
	 * set the port to send/receive Employees
	 * @param int : portServerEmployee
	 */
	public static void setPortServerEmployee(int portServerEmployee) {
		ConfigV2.portServerEmployee = portServerEmployee;
	}
	
	/**
	 * get the port for send/receive Pointings 
	 * @return int : the port
	 */
	public static int getPortServerPointing() {
		return portServerPointing;
	}
	
	/**
	 * set the port to send/receive Pointings
	 * @param int : portServerPointing
	 */
	public static void setPortServerPointing(int portServerPointing) {
		ConfigV2.portServerPointing = portServerPointing;
	}
	
	/**
	 * get the incident threshold 
	 * @return int
	 */
	public static int getAccidentThreshold() {
		return accidentThreshold;
	}
	
	/**
	 * set the incident threshold 
	 * @param : int
	 */
	public static void setAccidentThreshold(int accidentThreshold) {
		ConfigV2.accidentThreshold = accidentThreshold;
	}
	
	/**
	 * save all properties in a file .config
	 */
	public static void save() {
		Properties prop = new Properties();
		 
    	try {
    		//set the properties value
    		prop.setProperty("ipAdress", ipAdress);
    		prop.setProperty("portServerEmployee",  String.valueOf(portServerEmployee));
    		prop.setProperty("portServerPointing",  String.valueOf(portServerPointing));
    		prop.setProperty("accidentThreshold",  String.valueOf(accidentThreshold));
 
    		//save properties to project root folder
    		prop.store(new FileOutputStream(".config"), null);
 
    	} catch (IOException ex) {
    		ex.printStackTrace();
        }
	}
	
	/**
	 * get the properties from the .config file
	 */
	public static void update() {
		try {
			Properties prop = new Properties();
			InputStream input = new FileInputStream(".config");
			// load a properties file
			prop.load(input);
			ipAdress = prop.getProperty("ipAdress");
			portServerEmployee = Integer.parseInt(prop.getProperty("portServerEmployee"));
			portServerPointing = Integer.parseInt(prop.getProperty("portServerPointing"));
			accidentThreshold = Integer.parseInt(prop.getProperty("accidentThreshold"));
			input.close();
		} catch (Exception e) {
		}
	}
}
