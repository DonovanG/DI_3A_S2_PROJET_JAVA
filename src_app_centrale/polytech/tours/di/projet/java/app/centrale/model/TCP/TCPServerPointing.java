package polytech.tours.di.projet.java.app.centrale.model.TCP;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import polytech.tours.di.projet.java.app.centrale.controler.CompanyController;
import polytech.tours.di.projet.java.app.centrale.model.company.ConfigV2;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;

/**
 * the server TCP that is waiting for pointings
 * implement the runnable interface
 * @author donovan
 *
 */
public class TCPServerPointing implements Runnable{
	
	/** attributes */
	/** declare sockets */
	private ServerSocket ss; 
	private InetSocketAddress isA;
	/** declare pointing */
	private Pointing p;
	private CompanyController controller;
	
	public TCPServerPointing(CompanyController controller) {
		this.controller = controller;
		
		isA = new InetSocketAddress(ConfigV2.getIpAdress(),ConfigV2.getPortServerPointing());  

		try {
			ss = new ServerSocket(isA.getPort());
		} catch (IOException e) {
			System.exit(0);
		}
	}
	
	/**
	 * the run method of the runnable interface
	 * waiting for some pointings receive by the callable task
	 */
	@Override
	public void run() {
		System.out.println("TCPServerPoitingLaunched");
		
		ExecutorService executor = Executors.newFixedThreadPool(1);	
		
		while(true){
			try {
				try {
					p = executor.submit(new ServerPointing(ss.accept())).get();
					controller.getCompany().addpointing(p);
					System.out.println(p);
				} catch (ExecutionException | IOException e) {
					e.printStackTrace();
				}
			} catch (InterruptedException e2) {
				e2.printStackTrace();
			}
		}
				
	}
	
	/**
	 * @return the pointing
	 */
	public Pointing getPointing() {
		return p;
	}
	/**
	 * @param pointing the pointing to set
	 */
	public void setPointing(Pointing pointing) {
		this.p = pointing;
	}

	/**
	 * @return the ss
	 */
	public ServerSocket getSs() {
		return ss;
	}
	/**
	 * @param ss the ss to set
	 */
	public void setSs(ServerSocket ss) {
		this.ss = ss;
	}



}

