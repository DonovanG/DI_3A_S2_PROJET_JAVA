package polytech.tours.di.projet.java.app.centrale.vue;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import polytech.tours.di.projet.java.app.centrale.model.company.ConfigV2;

public class IncidentRenderer extends DefaultTableCellRenderer { 

	private static final long serialVersionUID = -3383905626007697221L;

	/**
	 * default cell render when creating a row in the JTable that implement IncidentRenderer
	 */
	public Component getTableCellRendererComponent(JTable table,
			Object value, boolean isSelected, boolean hasFocus, 
			int row, int column) { 
		
		Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); 
		
		int delay = 0;
		
		if(table.getModel().getValueAt(row, column) instanceof Integer)
			delay = (int) table.getModel().getValueAt(row, column);
				
		
		if(column == 3 && delay <= -(ConfigV2.getAccidentThreshold()))
			cell.setBackground(Color.red);
		else
			cell.setBackground(Color.white);

		return cell;
		
	} 
	
}
