package polytech.tours.di.projet.java.app.centrale.model.time;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

/**
 * create a date with it's hour rounding at the nearest fifteen minutes
 * @author donovan
 */
public class RoundedClock implements Serializable{
        
	/**
	 * 
	 */
	private static final long serialVersionUID = -1339960961519758232L;
	/** Attributes */
    private LocalDateTime ldt; /** < contain the date with year,month,day hour,minute */

    /**
     * constructor : round the current time to the nearest fifteen minutes
     */
    public RoundedClock() {
            ldt = LocalDateTime.now();
            this.roundedTime();
        }
    
    
    /**
     * constructor with parameters
     * @param YYYY
     * @param MM
     * @param DD
     * @param mm
     * @param hh 
     */
    public RoundedClock(int YYYY, int MM, int DD, int mm, int hh) {
            ldt = LocalDateTime.of(YYYY, MM, DD, hh, mm);
            this.roundedTime();
        }
    
    /**
     * constructor with a string formated to simplify de creation
     * @param str 
     */
    public RoundedClock(String str) {
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    	if(str.length() == 5)
    		str = "1111-11-11 "+str;
    	
    	ldt = LocalDateTime.parse(str, formatter);

	    this.roundedTime();
	}
    
    /**
     * the toString method
     * @return String 
     */
    @Override
        public String toString() {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                return ldt.format(formatter);
        }

     /**
     * rounded up to the nearest fifteen minute increment
     * @return LocalTime : the hour rounded
     */
    public LocalDateTime roundedTime (  )
    {
	    ldt = ldt.truncatedTo(ChronoUnit.MINUTES);
	    if(ldt.getMinute()%15<=7)
	        ldt = ldt.minusMinutes(ldt.getMinute()%15);
	    else
	        ldt = ldt.plusMinutes(15-ldt.getMinute()%15);
		return ldt;
        
    }
    
    /**
     * get the ldt attribute that correspond to the time
     * @return ldt 
     */
    public LocalDateTime getLdt() {
            return ldt;
    }

    /**
     * set the ldt attribut with a formated string
     * @param str 
     */
    public void setLdt(String str) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        ldt = LocalDateTime.parse(str, formatter);
        this.roundedTime();
    }

    /**
     * set the ldt attribut with a localDateTime
     * @param ldt 
     */
    public void setLdt(LocalDateTime ldt) {
            this.ldt = ldt;
            this.roundedTime();
    }
    
    /**
     * redifined the equals method by giving a calendar
     * @param calendar
     * @return true if the day, the month and the year are equals, false either
     */
    public boolean equals(Calendar calendar){
    	
    	if(getLdt().getDayOfMonth() == calendar.get(Calendar.DAY_OF_MONTH) &&
    			getLdt().getMonthValue() == calendar.get(Calendar.MONTH)+1 &&
    			getLdt().getYear() == calendar.get(Calendar.YEAR))
    		return true;
    	else
    		return false;
    }
    
    /**
     * compare 2 RoundedClock and return if thay are equals are not
     * @param clock
     * @return true if they are equals, false if they aren't
     */
    public boolean equals(RoundedClock clock){
    	if(clock.toString().equals(this.toString()))
    		return true;
    	return false;
    }
    
    /**
     * transform a RoundedClock into a java.util.Date
     * @return Date : the date
     */
    public Date toDate(){
    	return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
    }

}
 

