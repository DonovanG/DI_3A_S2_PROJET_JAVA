package polytech.tours.di.projet.java.app.centrale.model.company;
import polytech.tours.di.projet.java.app.centrale.controler.CompanyController;

/**
 * the main method of the main application
 * @author GUILLOT Donovan
 *
 */
public class MainJava {

	/** main method
	 * @param args
	 */
	public static void main(String[] args) {
		

		/** initialize the controler of the application */
		@SuppressWarnings("unused")
		CompanyController controler = new CompanyController();	

	}

}
