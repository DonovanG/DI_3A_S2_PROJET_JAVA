package polytech.tours.di.projet.java.app.centrale.model.TCP;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.concurrent.Callable;

import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;

/**
 * the server that receive the pointings 
 * implmeents the callable interface
 * @author donovan
 *
 */
public class ServerPointing implements Callable<Pointing>{
	/** the TCP socket */
	Socket s;
	/** the reading stream */
	ObjectInputStream ois;
	/** the pointing read */
	Pointing p;
	
	/**
	 * the constructor 
	 * @param s
	 */
	public ServerPointing(Socket s) {
		this.s = s;
	}

	/**
	 * the call method of the callable interface
	 * read a pointing from the reading stream 
	 */
	@Override
	public Pointing call(){
		System.out.println("ServerPointing reading...");
		
		try {
			ois = new ObjectInputStream(s.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			p = (Pointing) ois.readObject();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}

		System.out.println("ServerPointing reading done ...");
		
		try {
			ois.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return p;
	}

}
