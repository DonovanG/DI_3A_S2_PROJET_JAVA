/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polytech.tours.di.projet.java.app.centrale.test;



import org.junit.Assert;
import org.junit.Test;

import polytech.tours.di.projet.java.app.centrale.model.time.RoundedClock;
/**
 *	Test the class date
 * @author donovan
 */
public class DateTest{

    /**
     * test the rounding of the class RondedClock
     */
    @Test
    public void roundingTest(){
        RoundedClock roundedClock = new RoundedClock("2018-11-04 23:24");
        Assert.assertEquals(2018, roundedClock.getLdt().getYear());
        Assert.assertEquals(11, roundedClock.getLdt().getMonthValue());
        Assert.assertEquals(04, roundedClock.getLdt().getDayOfMonth());
        Assert.assertEquals(23, roundedClock.getLdt().getHour());
        Assert.assertEquals(30, roundedClock.getLdt().getMinute());
        
        roundedClock.setLdt("2018-11-04 23:47");
        Assert.assertEquals(45, roundedClock.getLdt().getMinute());
        
        roundedClock.setLdt("2018-11-04 23:55");
        Assert.assertEquals(5, roundedClock.getLdt().getDayOfMonth());
        Assert.assertEquals(00, roundedClock.getLdt().getMinute());
        Assert.assertEquals(00, roundedClock.getLdt().getHour());
        
        roundedClock.setLdt("2018-11-04 22:55");
        Assert.assertEquals(00, roundedClock.getLdt().getMinute());
        Assert.assertEquals(23, roundedClock.getLdt().getHour());   
    }
    
    
}
