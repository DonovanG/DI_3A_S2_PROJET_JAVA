package polytech.tours.di.projet.java.app.centrale.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import polytech.tours.di.projet.java.app.centrale.model.company.Company;
import polytech.tours.di.projet.java.app.centrale.model.company.Employee;
import polytech.tours.di.projet.java.app.centrale.model.time.RoundedClock;
import polytech.tours.di.projet.java.app.centrale.model.time.Pointing;

/**
 * Test the Pointing class
 * @author donovan
 *
 */
public class PointingTest {
	
    /** attributes */
    private Company company;
    private Employee employee;
    private Employee employee2;
    private Calendar calendar1;
	private Calendar calendar2;

	private Pointing pointing1;
	private Pointing pointing2;

	
	ArrayList<Pointing> pointings;
	
    @Before
    public void setUp(){
    	company = new Company();
    	company.addEmployee(new Employee("GUILLOT", "Donovan", "08:15", "17:00"), "IT");
    	company.addEmployee(new Employee("AUVRAY", "Damien", "08:00", "17:00"), "elec");

    	employee = company.getEmployee("GUILLOT", "Donovan");
    	employee2 = company.getEmployee("AUVRAY", "Damien");
    	
    }
    /** 
     * test addind some pointings 
     * @throws IOException
     */
    @Test
    public void CRUDPointingTest() throws IOException{

    	
    	company.addpointing(new Pointing(employee, new RoundedClock("2018-04-12 08:15")));
    	Assert.assertEquals(company.getHistory().getDay().get(0).getArriving().getDate().getLdt().getMinute(), 15);
    	Assert.assertEquals(company.getHistory().getDay().get(0).getLeaving(), null);
    	Assert.assertEquals(0, employee.getOvertime());

    	
    	company.addpointing(new Pointing(employee, new RoundedClock("2018-04-12 17:25")));
    	Assert.assertEquals(company.getHistory().getDay().get(0).getArriving().getDate().getLdt().getMinute(), 15);
    	Assert.assertEquals(company.getHistory().getDay().get(0).getLeaving().getDate().getLdt().getMinute(), 30);
    	Assert.assertEquals(30, employee.getOvertime());
    	
    	company.addpointing(new Pointing(employee, new RoundedClock("2018-05-12 08:15")));
    	Assert.assertEquals(company.getHistory().getDay().size(),2);
    	Assert.assertEquals(30, employee.getOvertime());
    	
    	company.addpointing(new Pointing(employee, new RoundedClock("2018-05-12 16:25")));
    	Assert.assertEquals(0, employee.getOvertime());
    	
    	/** test update */
    	company.updateEmployee(employee, employee2);
    	Assert.assertEquals(0, company.getHistory().getPointings(employee).size());
    	Assert.assertEquals(4, company.getHistory().getPointings(employee2).size());
    	
    	/** test remove */
    	company.removeEmployee(employee2.getName(), employee2.getFirstName());
    	Assert.assertEquals(0, company.getHistory().getPointings(employee2).size());

 	
    }
    
    @Test
    public void GetPointingsTest(){
    	company.addpointing(new Pointing(employee, new RoundedClock("2018-04-12 08:15")));
    	company.addpointing(new Pointing(employee, new RoundedClock("2018-04-13 17:15")));
    	company.addpointing(new Pointing(employee, new RoundedClock("2018-04-14 08:15")));
    	company.addpointing(new Pointing(employee, new RoundedClock("2018-04-15 17:15")));
    	company.addpointing(new Pointing(employee2, new RoundedClock("2018-04-16 08:15")));
    	company.addpointing(new Pointing(employee2, new RoundedClock("2018-04-17 17:15")));
    	company.addpointing(new Pointing(employee2, new RoundedClock("2018-04-18 08:15")));
    	company.addpointing(new Pointing(employee2, new RoundedClock("2018-04-19 17:15")));
    	
    	calendar1 = null;
    	calendar2 = null;
    	
    	pointings = company.getHistory().getPointings(calendar1, calendar2);
    	Assert.assertEquals(8, pointings.size());
    	pointings = company.getHistory().getPointings(employee, calendar1, calendar2);
    	Assert.assertEquals(4, pointings.size());
    	
    	calendar1 = new GregorianCalendar(2018,3,14);
    	
    	pointings = company.getHistory().getPointings(calendar1, calendar2);
    	Assert.assertEquals(6, pointings.size());
    	pointings = company.getHistory().getPointings(employee, calendar1, calendar2);
    	Assert.assertEquals(2, pointings.size());
    	pointings = company.getHistory().getPointings(employee2, calendar1, calendar2);
    	Assert.assertEquals(4, pointings.size());
    	
    	calendar2 = new GregorianCalendar(2018, 3, 18);
    	
    	pointings = company.getHistory().getPointings(calendar1, calendar2);
    	Assert.assertEquals(4, pointings.size());
    	pointings = company.getHistory().getPointings(employee, calendar1, calendar2);
    	Assert.assertEquals(2, pointings.size());
    	
    	calendar1 = null;

    	pointings = company.getHistory().getPointings(calendar1, calendar2);
    	Assert.assertEquals(6, pointings.size());
    	pointings = company.getHistory().getPointings(employee2, calendar1, calendar2);
    	Assert.assertEquals(2, pointings.size());

    	
    	/** remove Test **/
    	Assert.assertEquals(8, company.getHistory().getPointings().size());
    	company.getHistory().removePointing(company.getHistory().getDay().get(0).getArriving());
    	Assert.assertEquals(7, company.getHistory().getPointings().size());


    }
    
    @Test
    public void updatePointingTest(){
    	pointing1 = new Pointing(employee, new RoundedClock("2018-05-23 08:30"));
    	company.addpointing(pointing1);
    	pointing2 = new Pointing(employee, new RoundedClock("2018-05-23 16:45"));
    	company.addpointing(pointing2);
    	
    	Assert.assertEquals(2, company.getHistory().getPointings().size());
    	Assert.assertEquals(-30, employee.getOvertime());


    	company.updatePointing(pointing1, new RoundedClock("2018-05-23 08:45"));
    	
    	Assert.assertEquals(2, company.getHistory().getPointings().size());
    	Assert.assertEquals(-45, employee.getOvertime());
    	
    	company.updatePointing(pointing2, new RoundedClock("2018-05-23 16:30"));
    	
    	Assert.assertEquals(2, company.getHistory().getPointings().size());
    	Assert.assertEquals(-60, employee.getOvertime());


    }
    

    
}