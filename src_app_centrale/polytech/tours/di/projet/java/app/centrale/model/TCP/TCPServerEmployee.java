package polytech.tours.di.projet.java.app.centrale.model.TCP;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.ArrayList;

import polytech.tours.di.projet.java.app.centrale.model.company.ConfigV2;
import polytech.tours.di.projet.java.app.centrale.model.company.Employee;

/**
 * class that allow to receive packet containing the company's employees by TCT protocol
 * implements the runnable interface
 * @author donovan
 *
 */
public class TCPServerEmployee implements Runnable{
	/** declare sockets */
	InetSocketAddress isA;
	ServerSocket ss;
	/** declare the employee's list */
	ArrayList<Employee> employees;
	

	/**
	 * the constructor
	 * @param employees
	 */
	public TCPServerEmployee(ArrayList<Employee> employees){

		this.employees = employees;
		
		isA = new InetSocketAddress(ConfigV2.getIpAdress(),ConfigV2.getPortServerEmployee());  
		try {
			ss = new ServerSocket(isA.getPort());
		} catch (IOException e) {
			System.exit(0);
		} 
	}
	
	/**
	 * the run method of the runnable interface 
	 * make some connections and run some background threads that monitor the stream
	 */
	@Override
	public void run() {
		System.out.println("TCPServerEmployee Launched");
		
        while(true)
			try {
				new Thread(new ServerEmployee(ss.accept(), employees)).start();
			} catch (IOException e) {
				e.printStackTrace();
			}		
	}

}

