package polytech.tours.di.projet.java.app.centrale.model.TCP;

import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import polytech.tours.di.projet.java.app.centrale.model.company.Employee;

/**
 * the server wait for a client request to transfer the employee's list
 * implement runnable interface 
 * @author donovan
 *
 */
public class ServerEmployee implements Runnable {

	/** socket */
	Socket s;
	/** employee's list */
	ArrayList<Employee> employees;
	/** outputStream that will write the employee's list */
	ObjectOutputStream oos;
	
	/**
	 * the constructor
	 * @param s : the socket
	 * @param employees : the employee's list
	 */
	public ServerEmployee(Socket s , ArrayList<Employee> employees) {
		this.s = s;
		this.employees = employees;
	}
	
	/**
	 * the run method of the runnable interface
	 * write the employy's list in the stream 
	 */
	@Override
	public void run() {
		try {
			System.out.println("ServerEmployee transfering...");
			
			oos = new ObjectOutputStream(s.getOutputStream());
			
			oos.writeObject(employees);

			System.out.println("ServerEmployee transfer done ...");
			
			oos.close();
			s.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception ServerFile.");
		}		
	}

}
