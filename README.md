./src_app_centrale : Main software sources
./src_pointingMachine Poiting Machine sources
./diagram_UML : UML
./EmployeesFiles : .CSV file for importing and exporting employees and pointings
./lib : extern library used
./buid/classes : .class files
./company.ser & ./pointeuse.ser : serialization files
./doc : the javadoc of the project
****************************************************************************************************************************************
MVC : 

For each app, the controler contain the model and the view. Initialization of an object controler in each main.
All elements or initialize in the controler constructeur, like background threads, deserialization... the view is adding
as Observer to the model.
- The controler implement ActionListener and WindowListener to make interaction between the view and the model by clicking on a button
- The model implement the pattern Observable -> function setChanged and notifyObserver that allow to update the view
- The view implement the pattern Observer -> function update to populate tables and comboboxes
****************************************************************************************************************************************
Useful :

- No constraint on the order of launching.
- synchronized employee's list before check in
- If IP or ports modified in the "config" panel, please reboot the application.
- To set the leader of a department, create the department by adding it or by creating an employee
  then after selecting the department in the "dempartment panel" click on update and choose the manager.
- Option, you can :
	- open several pointing machine
	- check if a department has no manager
	- add, update, remove and import by csv some pointings
	- serialize IP,Ports,incidence threshold in a .config file
	- update an employee in a manager and reciprocally



